package space.trader;

import java.io.*;
import java.util.Random;
import javafx.beans.property.SimpleObjectProperty;
import space.trader.interfaceHelpers.*;

/**
* Creates objects needed for the game to run, as well as handling screen switching
*
* @author Robert
*/
public class MainGameController implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final IDisplayControl displayController;
    private IPlayer player;
    private IGalaxy galaxy;
    private final Random rnd;
    
    private SimpleObjectProperty<IPlanet> currentSystem;
    
    /**
    * The only constructor for this class that takes in an IDisplayControl
    *   object that will actually change the screen for the
    *   MainGameController.
    *
    * @param displayControl The IDisplayControl that will change the screens
    *   the player will see.
    */
    public MainGameController(final IDisplayControl displayControl)
    {
        displayController = displayControl;
        rnd = new Random();
    }
    
    /**
    * Tells the displayController to change the screen being shown to the
    *   MarketplaceScreen.
    */
    public void loadMarketplaceScreen()
    {
        displayController.loadMarketplaceScreen(player, currentSystem, this);
    }

    /**
    * Tells the displayController to change the screen being shown to the
    *   CreateNewGame.
    */
    public void loadCreateGameScreen() { displayController.loadCreateGameScreen(this); }

    /**
    * Tells the displayController to change the screen being shown to the
    *   MapTravelScreen.
    */
    public void loadMapTravelScreen()
    {
        displayController.loadMapTravelScreen(player, galaxy, currentSystem, this, rnd); 
    }

    /**
     * Tells the displayController to change the screen being shown to the 
     *   CombatScreen.
     */
    public void loadCombatScreen() {
        displayController.loadCombatScreen(player, this);
    }

    /**
    * Tells the displayController to change the screen being shown to the
    *   StartupScreen.
    */
    public void start() { displayController.loadStartScreen(this); }

    /**
     * Called to initialize a new game, sets the starting planet and calls the
     * map travel screen.
     *
     * @param player
     * @param galaxy
     */
    public void createNewGameStart(final IPlayer player, final IGalaxy galaxy)
    {
        this.player = player;
        this.galaxy = galaxy;

        //Set random planet as our starting system
        currentSystem = new SimpleObjectProperty<IPlanet>(
                galaxy.getSolarSystems().get(
                rnd.nextInt(galaxy.getSolarSystems().size())
        ));

        loadMapTravelScreen();
    }

    /**
     * Called to update the current planet the player is on.
     *
     * @param planet
     */
    public void travelToPlanet(final IPlanet planet)
    {
        this.currentSystem.set(planet);
    }
    
    /**
     * Handles the saving of a currently running game. Does so using Streams.
     * 
     * @param saveFile 
     */
    public void saveGame(final File saveFile)
    {
        try
        {
            final FileOutputStream fileStream = new FileOutputStream(saveFile);
            final ObjectOutputStream objStream = new ObjectOutputStream(fileStream);
            
            objStream.writeObject(player);
            //objStream.writeObject(SpaceTrader.getCurrentPlanet());
            //objStream.writeObject(SpaceTrader.getGalaxy());
            //objStream.writeInt(CargoBay.capacityProperty().get());
            //ArrayList<TradeGood> goods = new ArrayList<>();
            //goods.addAll(CargoBay.getInventory());
            //objStream.writeObject(goods);
        }
        catch(Exception ex)
        {
            System.err.println("Save game error");
            ex.printStackTrace();
        }
        
    }
    
    /**
     * Handles the opening of a file that was previously saved. 
     * 
     * @param openFile 
     */
    public void openGame(final File openFile)
    {
        try
        {
            final FileInputStream fileStream = new FileInputStream(openFile);
            final ObjectInputStream objStream = new ObjectInputStream(fileStream);
            
            //SpaceTrader.setPlayer((Player)objStream.readObject());
            //SpaceTrader.setCurrentPlanet((SolarSystem)objStream.readObject());
            //SpaceTrader.setGalaxy((Galaxy)objStream.readObject());
            //CargoBay.capacityProperty().set(objStream.readInt());
            //CargoBay.getInventory().addAll((ArrayList<TradeGood>)objStream.readObject());
            
            loadMapTravelScreen();
        }
        catch (Exception ex)
        {
            System.err.println("Open game error");
            ex.printStackTrace();
        }
                
    }
}
