package space.trader;

import javafx.application.Application;
import javafx.stage.Stage;
import space.trader.view.DisplayController;

/**
 * The class that holds the main information for the game that is instantiated
 *   when the game is created, such as the player, the galaxy, and the player's
 *   location.
 *
 * @author Robert
 */
public class SpaceTrader extends Application
{
    /**
     * The method that starts the Application and initializes the
     *   MainGameController with the stage, and starts the game.
     *
     * @param stage The Stage that will display everything for the game.
     * @throws Exception
     */
    @Override
    public void start(final Stage stage) throws Exception
    {
        final MainGameController gameControl = new MainGameController(new DisplayController(stage));
        gameControl.start();
    }

    /**
     * The main method.
     *
     * @param args the command line arguments
     */
    public static void main(final String[] args)
    {
        launch(args);
    }
}
