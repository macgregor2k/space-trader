package space.trader.interfaceHelpers;

import java.util.Random;
import javafx.beans.property.ReadOnlyObjectProperty;
import space.trader.MainGameController;

/**
 * An interface that specifies the various screens of the game and loads them
 * in a non-implementation specific way. The actual implemented class in the
 * view package will extend this, allowing dependency injection of the view structure
 * into the main game program.
 *
 * @author Robert
 */
public interface IDisplayControl
{
    void loadCreateGameScreen(MainGameController gameControl);

    void loadMapTravelScreen(IPlayer player, IGalaxy galaxy,
            ReadOnlyObjectProperty<IPlanet> currentSystem,
            MainGameController gameControl, Random rnd);

    void loadMarketplaceScreen(IPlayer player,
            ReadOnlyObjectProperty<IPlanet> currentSystem,
            MainGameController gameControl);

    void loadStartScreen(MainGameController gameControl);

    void loadCombatScreen(IPlayer player,
            MainGameController gameControl);
}
