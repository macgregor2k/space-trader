package space.trader.interfaceHelpers;

import javafx.collections.ObservableList;

/**
 *
 * @author Robert
 */
public interface IGalaxy
{

    /**
     *  A getter method that returns the ArrayList holding all of the
     *   SolarSystems in the Galaxy.
     *
     * @return The ArrayList containing every SolarSystem in the Galaxy.
     */
    ObservableList<IPlanet> getSolarSystems();

    /**
     * The toString method that returns each SolarSystem in the Galaxy.
     *
     * @return A String containing each SolarSystem in the Galaxy on a
     *   separate line.
     */
    String toString();
    
}
