package space.trader.interfaceHelpers;

import javafx.geometry.Point2D;
import space.trader.model.Marketplace;
import space.trader.model.GadgetStore;
import space.trader.model.Shipyard;

/**
 *
 * @author Robert
 */
public interface IPlanet
{
    Point2D getLocation();
    Marketplace getMarket();
    GadgetStore getGadgetStore();
    String getName();
    Resource getResource();
    Tech_Level getTechLevel();
    Shipyard getShipyard();
    
    @SuppressWarnings("PublicInnerClass")
    public static enum Tech_Level
    {
        PRE_AGRICULTURE ("Pre-Agriculture"),
        AGRICULTURE ("Agricultural"),
        MEDIEVAL ("Medieval"),
        RENAISSANCE ("Renassaince"),
        EARLY_INDUSTRIAL ("Early Industrial"),
        INDUSTRIAL ("Industrial"),
        POST_INDUSTRIAL ("Post-Industrial"),
        HI_TECH ("Hi-Tech");
        
        private final String Name;
        
        private Tech_Level(String Name) { this.Name = Name; }
        
        /**
         * Displays the name of this instance of Tech_Level.
         *
         * @return The String holding the name of this instance of Tech_Level.
         */
        String displayName() { return Name; }
    }
    
    @SuppressWarnings("PublicInnerClass")
    public static enum Resource
    {
        NO_SPECIAL_RESOURCE ("No Special Resources"),
        MINERAL_RICH ("Mineral Rich"),
        MINERAL_POOR ("Mineral Poor"),
        DESERT ("Desert"),
        LOTS_OF_WATER ("Lots of Water"),
        RICH_SOIL ("Rich Soil"),
        POOR_SOIL ("Poor Soil"),
        RICH_FAUNA ("Rich Fauna"),
        LIFELESS ("Lifeless"),
        WEIRDSHROOM ("Weird Mushrooms"),
        LOTS_OF_HERBS ("Lots of Herbs"),
        ARTISTIC ("Artistic"),
        WARLIKE ("War-Like");
        
        private final String Name;
        
        private Resource(String Name) { this.Name = Name; }
        
        /**
         * Displays the name of this instance of Resource.
         *
         * @return The String holding the name of this instance of Resource.
         */
        String displayName() { return Name; }
    }
}
