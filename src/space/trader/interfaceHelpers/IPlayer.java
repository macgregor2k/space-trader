package space.trader.interfaceHelpers;

import javafx.beans.property.*;
import space.trader.model.PlayerShip;

/**
 *
 * @author Robert
 */
public interface IPlayer
{
    //Property accessors
    SimpleIntegerProperty pilotPointsProperty();
    SimpleDoubleProperty currencyProperty();
    SimpleIntegerProperty engineerPointsProperty();
    SimpleIntegerProperty investorPointsProperty();
    SimpleIntegerProperty tacticalPointsProperty();
    SimpleIntegerProperty traderPointsProperty();
    SimpleObjectProperty<PlayerShip> shipProperty();
    
    //Getters
    String getName();
    int getPilotPoints();
    int getTacticalPoints();
    int getTraderPoints();
    double getCurrency();
    int getEngineerPoints();
    int getInvestorPoints();
    PlayerShip getShip();

    //Setters
    void setPilotPoints(int pilotPoints);
    void setTacticalPoints(int tacticalPoints);
    void setTraderPoints(int traderPoints);
    void setEngineerPoints(int engineerPoints);
    void setInvestorPoints(int investorPoints);
    void setShip(PlayerShip ship);

    /**
     * Adds an amount of currency to the Player's money.
     *
     * @param currency The amount to be added to the Player's money.
     */
    void addCurrency(double currency);

    /**
     * Removes an amount of currency from the Player's money.
     *
     * @param currency The amount to be removed from the Player's money.
     */
    void removeCurrency(double currency);
    
    /**
     * The toString method returns the Player's name, along with the number of
     *   skill points that were allocated in each of the possible categories.
     *
     * @return A String containing the Player's name and how the skill points
     *   were spent.
     */
    String toString();
}
