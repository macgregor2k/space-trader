package space.trader.model;

import java.util.Collection;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The CargoBay holds TradeGoods and has a capacity that dictates how many
 *   items it can hold at one time.
 * 
 * @author Robert
 */
public class CargoBay
{
    private final ObservableList<TradeGood> inventory;
    private final ReadOnlyIntegerWrapper capacity;
    private int numGoods;
    
    public CargoBay(final int capacity)
    {
        this.capacity = new ReadOnlyIntegerWrapper(capacity);
        this.inventory = FXCollections.observableArrayList();
    }
    
    public CargoBay(final int capacity, final Collection<TradeGood> cargo, final int prevNumGoods)
    {
        this.capacity = new ReadOnlyIntegerWrapper(capacity);
        if (capacity >= cargo.size())
        {
            this.inventory = FXCollections.observableArrayList(cargo);
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
        numGoods = prevNumGoods;
    }

    /**
     * Gets the property holding the CargoBay's total capacity.
     *
     * @return A ReadOnlyIntegerProperty holding the total capacity of the
     *   current CargoBay.
     */
    public ReadOnlyIntegerProperty capacityProperty()
    {
        return capacity.getReadOnlyProperty();
    }

    /**
     * Changes the total capacity of the player's CargoBay.
     *
     * @param newCapacity The new maximum capacity of the CargoBay.
     */
    public void setCapacity(final int newCapacity) {
        capacity.set(newCapacity);
    }

    /**
     * The total amount of goods in the CargoBay.
     *
     * @return An int representing the total amount of TradeGoods the player
     *   currently owns.
     */
    public int getNumGoods() {
        return numGoods;
    }

    /**
     * Attempts to add a quantity of a TradeGood to the inventory.
     * 
     * @param good The TradeGood that is trying to be added to the inventory.
     * @param quantity The quantity of a TradeGood that is attempting to be
     *   added to the inventory.
     */
    public void addGoodQuantity(final TradeGood good, final int quantity)
    {
        if (quantity <= capacity.get() - numGoods)
        {
            if (inventory.contains(good)) 
            {
                TradeGood syncItem = null; //added to avoid sync issues
                for(TradeGood item : inventory)
                {
                    if (item.equals(good))
                    {
                        syncItem = item;
                    }
                }
                if (syncItem != null)
                {
                    syncItem.addQuantity(quantity);
                }
            } 
            else 
            {
                final TradeGood newItem = new TradeGood(good);
                newItem.quantityProperty().set(quantity);
                inventory.add(newItem);
            }
            numGoods = numGoods + quantity;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Attempts to remove a quantity of a TradeGood from the inventory.
     * 
     * @param good The TradeGood that is trying to be removed from the
     *   inventory.
     * @param quantity The quantity of a TradeGood that is attempting to be
     *   removed from the inventory.
     */
    public void removeGoodQuantity(final TradeGood good, final int quantity)
    {
        if (quantity <= good.getQuantity() && quantity > 0)
        {
            TradeGood syncItem = null; //added to avoid sync issues
            for(TradeGood item : inventory)
            {
                if (item.equals(good))
                {
                    syncItem = item;
                }
            }
            if (syncItem != null)
            {
                syncItem.removeQuantity(quantity);
                if (syncItem.getQuantity() == 0)
                {
                    inventory.remove(syncItem);
                }
                numGoods = numGoods - quantity;
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Gets the inventory collection
     * 
     * @return an unmodifiable version of the backing Observable list
     */
    public ObservableList<TradeGood> getInventory()
    {
        return FXCollections.unmodifiableObservableList(inventory);
    } 
}
