package space.trader.model;

import java.io.*;
import java.util.Objects;
import javafx.beans.property.*;
import space.trader.interfaceHelpers.IPlanet.Tech_Level;

/**
 * The gadgets that can be bought and sold
 *
 * @author andrewamontree
 */
public class Gadget implements Serializable
{
    private static final long serialVersionUID = 1L;
    transient private SimpleObjectProperty<GadgetType> type;
    transient private SimpleIntegerProperty quantity;
    transient private ReadOnlyDoubleWrapper buyPrice;
    transient private ReadOnlyDoubleWrapper sellPrice;
    
    public Gadget(GadgetType type, SolarSystem planet)
    {
        this.type = new SimpleObjectProperty<>(type);
        this.quantity = new SimpleIntegerProperty(1);
        this.buyPrice = new ReadOnlyDoubleWrapper(type.calculateBuyValue(planet));
        this.sellPrice = new ReadOnlyDoubleWrapper(type.calculateSellValue(planet));
    }
    
    public Gadget(Gadget good)
    {
        this.type = new SimpleObjectProperty<>(good.getType());
        this.quantity = new SimpleIntegerProperty(good.getQuantity());
        this.buyPrice = new ReadOnlyDoubleWrapper(good.getBuyPrice());
        this.sellPrice = new ReadOnlyDoubleWrapper(good.getSellPrice()); 
    }
    
    //Properties
    /**
     * Accesses the property of this Gadget's type.
     * 
     * @return The SimpleObjectProperty of this Gadget's type.
     */
    public SimpleObjectProperty<GadgetType> typeProperty() { return type; }
    
    /**
     * Accesses the property of this Gadget's quantity.
     * 
     * @return The SimpleIntegerProperty of this Gadget's quantity.
     */
    public SimpleIntegerProperty quantityProperty() { return quantity; }
    
    /**
     * Accesses the property of this Gadget's buy price.
     * 
     * @return The ReadOnlyDoubleProperty of this Gadget's buy price.
     */
    public ReadOnlyDoubleProperty buyPriceProperty() { return buyPrice.getReadOnlyProperty(); }
    
    /**
     * Accesses the property of this Gadget's sell price.
     * 
     * @return The ReadOnlyDoubleProperty of this Gadget's sell price.
     */
    public ReadOnlyDoubleProperty sellPriceProperty() { return sellPrice.getReadOnlyProperty(); }
    
    //Getters
    /**
     * Gets the type of this Gadget.
     * 
     * @return The GadgetType of this Gadget.
     */
    public GadgetType getType() { return type.getValue(); }
    
    /**
     * Gets the quantity of this Gadget.
     * 
     * @return An int representing the quantity of this Gadget.
     */
    public int getQuantity() { return quantity.get(); }
    
    /**
     * Gets the buy price of this Gadget.
     * 
     * @return A double representing the buy price of this Gadget.
     */
    public double getBuyPrice() { return buyPrice.get(); }
    
    /**
     * Gets the sell price of this Gadget.
     * 
     * @return A double representing the sell price of this Gadget.
     */
    public double getSellPrice() { return sellPrice.get(); }
    
    //Setters
    /**
     * Increases the quantity of a Gadget by a certain amount.
     * 
     * @param quantity The amount to add to the current quantity of this
     *   Gadget.
     */
    public void addQuantity(int quantity) 
    { 
        this.quantity.set(this.quantity.get() + quantity); 
    }
    
    /**
     * Decreases the quantity of a Gadget by a certain amount.
     * 
     * @param quantity The amount to remove from the current quantity of this
     *   Gadget.
     */
    public void removeQuantity(int quantity) 
    { 
        this.quantity.set(this.quantity.get() - quantity);
        if (this.quantity.get() < 0)
            throw new IndexOutOfBoundsException();
    }
    
    /**
     * Compares this Gadget with an Object to see if they are equal to each
     *   other.
     * 
     * @param o The Object to see if this Gadget is equal to.
     * @return The boolean true if the parameter is equal to this Gadget,
     *   and false it they are not equal.
     */
    @Override
    public boolean equals(Object o)
    {
        if (o == this)
            return true;
        
        if (o == null || o.getClass() != this.getClass())
            return false;
        
        return ((Gadget)o).getType().equals(this.getType());
    }

    /**
     * Creates and returns the hash code for this Gadget.
     * 
     * @return An int representing this Gadget's hash code.
     */
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.type);
        return hash;
    }
    
    
    @SuppressWarnings("PublicInnerClass")
    public static enum GadgetType
    {
        CLOAK ("Cloak", Tech_Level.HI_TECH, 30000),
        AUTOREPAIR ("Auto-Repair System", Tech_Level.HI_TECH, 15000),
        NAVIGATIONSYSTEM ("Navigation System", Tech_Level.POST_INDUSTRIAL, 10000),
        TARGETINGSYSTEM ("Targeting System", Tech_Level.POST_INDUSTRIAL, 5000),
        CARGOBAYUPGRADE ("Upgrade Cargo Bay", Tech_Level.POST_INDUSTRIAL, 1500);
        
        private final String displayName;
        private final Tech_Level minTechProduction;
        private final double basePrice;
        
        private GadgetType(String displayName, Tech_Level MTLP, 
                int BP)
        {
            this.displayName = displayName;
            this.minTechProduction = MTLP;
            this.basePrice = BP;

        }

        /**
         * Returns the cost of buying a gadget from this solarSystem.
         *
         * @param currentSystem The SolarSystem the player is currently on.
         * @return A double holding the price to buy a specific gadget in this
         *   SolarSystem.
         */
        public double calculateBuyValue(SolarSystem currentSystem)
        {
            return basePrice;
        }

        /**
         * Returns the amount of money the player gets back for selling a gadget
         *   in this SolarSystem.
         *
         * @param currentSystem The SolarSystem the player is currently in.
         * @return A double holding the amount of money the player will get for
         *   selling a gadget.
         */
        public double calculateSellValue(SolarSystem currentSystem)
        {
            return basePrice;
        }

        /**
         * This toString method returns the name of this Gadget.
         * 
         * @return A String holding the name of this Gadget.
         */
        @Override
        public String toString()
        {
            return displayName;
        }
        
        public Tech_Level getMinTechProduction() { return minTechProduction; }
    }
    
    private void writeObject(ObjectOutputStream os) throws IOException
    {
        os.defaultWriteObject();
        os.writeObject(type.get());
        os.writeInt(quantity.get());
        os.writeDouble(buyPrice.get());
        os.writeDouble(sellPrice.get());
    }
    
    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
    {
        is.defaultReadObject();
        type = new SimpleObjectProperty<>((GadgetType)is.readObject());
        quantity = new SimpleIntegerProperty(is.readInt());
        buyPrice = new ReadOnlyDoubleWrapper(is.readDouble());
        sellPrice = new ReadOnlyDoubleWrapper(is.readDouble()); 
    }
    
}
