package space.trader.model;

import java.util.Collection;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The GadgetBay holds Gadgets and has a capacity that dictates how many
 *   items it can hold at one time.
 * 
 * @author andrewamontree
 */
public class GadgetBay
{
    private final ObservableList<Gadget> inventory;
    private final ReadOnlyIntegerWrapper capacity;
    private int numGadgets;
    
    public GadgetBay(final int capacity)
    {
        this.capacity = new ReadOnlyIntegerWrapper(capacity);
        this.inventory = FXCollections.observableArrayList();
    }

    public GadgetBay(final int capacity, final Collection<Gadget> gadgetBay, final int prevNumEquipment)
    {
        this.capacity = new ReadOnlyIntegerWrapper(capacity);
        if (capacity >= gadgetBay.size())
        {
            this.inventory = FXCollections.observableArrayList(gadgetBay);
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
        numGadgets = prevNumEquipment;
    }

    /**
     * Returns the property holding the GadgetBay's current capacity.
     *
     * @return A ReadOnlyIntegerProperty containing the current capacity of the
     *   GadgetBay.
     */
    public ReadOnlyIntegerProperty capacityProperty()
    {
        return capacity.getReadOnlyProperty();
    }

    /**
     * Sets the capacity of the GadgetBay.
     *
     * @param newCapacity The new capacity of the GadgetBay.
     */
    public void setCapacity(final int newCapacity) 
    {
        capacity.set(newCapacity);
    }

    /**
     * Gets the number of Gadgets that are in the GadgetBay.
     *
     * @return An int representing the number of Gadgets the player currently
     *   owns.
     */
    public int getNumGadgets() 
    {
        return numGadgets;
    }
    
    /**
     * Attempts to add a quantity of a Gadget to the gadgets.
     * 
     * @param gadget The Gadget that is trying to be added to the gadgets.
     * @param quantity The quantity of a Gadget that is attempting to be
     *   added to the gadgets.
     */
    public void addGadgetQuantity(final Gadget gadget, final int quantity)
    {
        if (quantity <= capacity.get() - numGadgets && quantity > 0)
        {
            if (inventory.contains(gadget)) 
            {
                Gadget syncItem = null; //added to avoid sync issues
                for(Gadget item : inventory)
                {
                    if (item.equals(gadget))
                    {
                        syncItem = item;
                    }
                }
                if (syncItem != null)
                {
                    syncItem.addQuantity(quantity);
                }
            } 
            else 
            {
                final Gadget newItem = new Gadget(gadget);
                newItem.quantityProperty().set(quantity);
                inventory.add(newItem);
            }
            numGadgets = numGadgets + quantity;
        }
        else
        {
            throw new IndexOutOfBoundsException("Capacity is only: " + capacity.get());
        }
    }
    
    /**
     * Attempts to remove a quantity of a Gadget from the gadgets.
     * 
     * @param gadget The Gadget that is trying to be removed from the
     *   gadgets.
     * @param quantity The quantity of a Gadget that is attempting to be
     *   removed from the gadgets.
     */
    public void removeGadgetQuantity(final Gadget gadget, final int quantity)
    {
        if (quantity <= gadget.getQuantity() && quantity > 0)
        {
            Gadget syncItem = null; //added to avoid sync issues
            for(Gadget item : inventory)
            {
                if (item.equals(gadget))
                {
                    syncItem = item;
                }
            }
            if (syncItem != null)
            {
                syncItem.removeQuantity(quantity);
                if (syncItem.getQuantity() == 0)
                {
                    inventory.remove(syncItem);
                }
            }
            numGadgets = numGadgets - quantity;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }
    
    /**
     * Gets the inventory collection
     * 
     * @return an unmodifiable version of the backing Observable list
     */
    public ObservableList<Gadget> getInventory()
    {
        return FXCollections.unmodifiableObservableList(inventory);
    }
}
