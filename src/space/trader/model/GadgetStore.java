package space.trader.model;

import java.io.*;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import space.trader.interfaceHelpers.IPlanet;
import space.trader.model.Gadget.GadgetType;

/**
 * A class that deals with most of the logic for buying and selling Gadgets
 *   at a planet's GadgetStore.
 * 
 * @author andrewamontree
 */
public class GadgetStore implements Serializable
{    
    private static final long serialVersionUID = 1L;
    private transient ObservableList<Gadget> gadgets;
    
    /**
     * Instantiates a marketplace for a specific planet.
     * 
     * @param system The SolarSystem this marketplace is in.
     */
    public GadgetStore(SolarSystem system)
    {
        gadgets = FXCollections.observableArrayList();
        
        for (GadgetType gadget : GadgetType.values())
        {
            if (gadget.getMinTechProduction().equals(IPlanet.Tech_Level.POST_INDUSTRIAL)
                    && (system.getTechLevel().equals(IPlanet.Tech_Level.POST_INDUSTRIAL)
                    || system.getTechLevel().equals(IPlanet.Tech_Level.HI_TECH))) 
            {
                Gadget newGadget = new Gadget(gadget, system);
                gadgets.add(newGadget);
            }
            else if (system.getTechLevel().equals(IPlanet.Tech_Level.HI_TECH)) 
            {
                Gadget newGadget = new Gadget(gadget, system);
                gadgets.add(newGadget);
            }
        }
    }
    
    /**
     * Removes a quantity of Gadgets from the GadgetStore
     * 
     * @param gadget The Gadget to be removed from the GadgetStore.
     * @param quantity The quantity of the Gadget in question to be removed.
     */
    public void removeGadgetQuantity(Gadget gadget, int quantity)
    {
        if (quantity <= gadget.getQuantity() && quantity > 0)
        {
            Gadget syncItem = null; //added to avoid sync issues
            for(Gadget item : gadgets)
            {
                if (item.equals(gadget))
                    syncItem = item;
            }
            if (syncItem != null)
            {
                syncItem.removeQuantity(quantity);
                if (syncItem.getQuantity() == 0)
                    gadgets.remove(syncItem);
            }
        }
        else
            throw new IndexOutOfBoundsException();
    }
    
    /**
     * Adds a quantity of Gadgets into the GadgetStore
     * 
     * @param gadget The Gadget to be added to the GadgetStore.
     * @param quantity The quantity of the Gadget in question to be added.
     */
    public void addGadgetQuantity(Gadget gadget, int quantity)
    {
        if (quantity > 0)
        {
            if (gadgets.contains(gadget)) 
            {
                Gadget syncItem = null; //added to avoid sync issues
                for(Gadget item : gadgets)
                {
                    if (item.equals(gadget))
                        syncItem = item;
                }
                if (syncItem != null)
                    syncItem.addQuantity(quantity);
            } 
            else 
            {
                Gadget newItem = new Gadget(gadget);
                newItem.quantityProperty().set(quantity);
                gadgets.add(newItem);
            }
        }
    }
    
    /**
     * Gets the Gadgets and their details that are currently in the
     *   GadgetStore.
     * 
     * @return An ObservableList containing all the Gadgets remaining in the
     *   GadgetStore.
     */
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public ObservableList<Gadget> getAvailableGadgets()
    {
        return gadgets;
    }
    
    // transient private final ObservableList<Gadget> gadgets;
    private void writeObject(ObjectOutputStream os) throws IOException
    {
        os.defaultWriteObject();
        @SuppressWarnings("LocalVariableHidesMemberVariable")
        ArrayList<Gadget> gadgets = new ArrayList<>();
        gadgets.addAll(gadgets);
        os.writeObject(gadgets);
    }
    
    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
    {
        is.defaultReadObject();
        gadgets = FXCollections.observableArrayList((ArrayList<Gadget>)is.readObject());
    }
    
}
