package space.trader.model;

import java.io.Serializable;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import space.trader.interfaceHelpers.IGalaxy;
import space.trader.interfaceHelpers.IPlanet;

/**
 * The class that generates and contains the SolarSystems that make up the
 *   galaxy.
 *
 * @author Robert
 */
public class Galaxy implements Serializable, IGalaxy
{
    private static final int GALAXY_SIZE_X = 1000;
    private static final int GALAXY_SIZE_Y = 1500;
    private static final int GALAXY_DENSITY = 25; // # of solar systems in galaxy
    private static final long serialVersionUID = 1L;
    
    private final ObservableList<IPlanet> solarSystems;
    
    /**
     * Instantiates a new galaxy and fills it with SolarSystems.
     */
    public Galaxy()
    {
        final String[] SolarSystemNames = createSolarSystemNames();
        solarSystems = FXCollections.observableArrayList();
        generateGalaxy(SolarSystemNames);
    }
    
    /**
     * The toString method that returns each SolarSystem in the Galaxy.
     * 
     * @return A String containing each SolarSystem in the Galaxy on a
     *   separate line.
     */
    @Override
    public String toString()
    {
        String returnString = "Galaxy List:\n";
        for (IPlanet planet : solarSystems)
        {
            returnString += planet + "\n";
        }
        
        return returnString + "\n";
    }
    
    /**
     *  A getter method that returns the ArrayList holding all of the
     *   SolarSystems in the Galaxy.
     * 
     * @return The ArrayList containing every SolarSystem in the Galaxy.
     */
    @Override
    public ObservableList<IPlanet> getSolarSystems()
    {
        return FXCollections.unmodifiableObservableList(solarSystems);
    }
    
    /**
     * Generates the galaxy by creating SolarSystems with unique locations and
     *   attributes.
     *
     * @param solarSystemNames The candidates for the names of SolarSystems.
     */
    private void generateGalaxy(final String[] solarSystemNames)
    {
        final Random rnd = new Random();
        
        final Set<Point2D> locationSet = new HashSet<Point2D>();
        while (locationSet.size() < GALAXY_DENSITY)
        {
            locationSet.add(new Point2D(rnd.nextInt(GALAXY_SIZE_X / 10) * 10,
                                    rnd.nextInt(GALAXY_SIZE_Y / 10) * 10));
        }
        
        final Set<String> namesSet = new HashSet<String>();
        while (namesSet.size() < GALAXY_DENSITY)
        {
            namesSet.add(solarSystemNames[rnd.nextInt(solarSystemNames.length)]);
        }
        
        final Point2D[] locations = locationSet.toArray(new Point2D[locationSet.size()]);
        final String[] names = namesSet.toArray(new String[namesSet.size()]);
        
        for(int i = 0; i < GALAXY_DENSITY; i++)
        {
            solarSystems.add(new SolarSystem(locations[i], names[i], rnd));
        }
    }
    
    /**
     * Contains and returns the list of names to choose from when naming
     *   SolarSystems.
     *
     * @return A String[] containing potential names for SolarSystems.
     */
    private String[] createSolarSystemNames()
    {
        // Many of these names are from Star Trek: The Next Generation, or are small changes
        // to names of this series. A few have different origins.
        final String[] SolarSystemName =
        {
            "Acamar",
            "Adahn",            // The alternate personality for The Nameless One in "Planescape: Torment"
            "Aldea",
            "Andevian",
            "Antedi",
            "Balosnee",
            "Baratas",
            "Brax",             // One of the heroes in Master of Magic
            "Bretel",           // This is a Dutch device for keeping your pants up.
            "Calondia",
            "Campor",
            "Capelle",          // The city I lived in while programming this game
            "Carzon",
            "Castor",           // A Greek demi-god
            "Cestus",
            "Cheron",
            "Courteney",        // After Courteney Cox…
            "Daled",
            "Damast",
            "Davlos",
            "Deneb",
            "Deneva",
            "Devidia",
            "Draylon",
            "Drema",
            "Endor",
            "Esmee",            // One of the witches in Pratchett's Discworld
            "Exo",
            "Ferris",           // Iron
            "Festen",           // A great Scandinavian movie
            "Fourmi",           // An ant, in French
            "Frolix",           // A solar system in one of Philip K. Dick's novels
            "Gemulon",
            "Guinifer",         // One way of writing the name of king Arthur's wife
            "Hades",            // The underworld
            "Hamlet",           // From Shakespeare
            "Helena",           // Of Troy
            "Hulst",            // A Dutch plant
            "Iodine",           // An element
            "Iralius",
            "Janus",            // A seldom encountered Dutch boy's name
            "Japori",
            "Jarada",
            "Jason",            // A Greek hero
            "Kaylon",
            "Khefka",
            "Kira",             // My dog's name
            "Klaatu",           // From a classic SF movie
            "Klaestron",
            "Korma",            // An Indian sauce
            "Kravat",           // Interesting spelling of the French word for "tie"
            "Krios",
            "Laertes",          // A king in a Greek tragedy
            "Largo",
            "Lave",             // The starting system in Elite
            "Ligon",
            "Lowry",            // The name of the "hero" in Terry Gilliam's "Brazil"
            "Magrat",           // The second of the witches in Pratchett's Discworld
            "Malcoria",
            "Melina",
            "Mentar",           // The Psilon home system in Master of Orion
            "Merik",
            "Mintaka",
            "Montor",           // A city in Ultima III and Ultima VII part 2
            "Mordan",
            "Myrthe",           // The name of my daughter
            "Nelvana",
            "Nix",              // An interesting spelling of a word meaning "nothing" in Dutch
            "Nyle",             // An interesting spelling of the great river
            "Odet",
            "Og",               // The last of the witches in Pratchett's Discworld
            "Omega",            // The end of it all
            "Omphalos",         // Greek for navel
            "Orias",
            "Othello",          // From Shakespeare
            "Parade",           // This word means the same in Dutch and in English
            "Penthara",
            "Picard",           // The enigmatic captain from ST:TNG
            "Pollux",           // Brother of Castor
            "Quator",
            "Rakhar",
            "Ran",              // A film by Akira Kurosawa
            "Regulas",
            "Relva",
            "Rhymus",
            "Rochani",
            "Rubicum",          // The river Ceasar crossed to get into Rome
            "Rutia",
            "Sarpeidon",
            "Sefalla",
            "Seltrice",
            "Sigma",
            "Sol",              // That's our own solar system
            "Somari",
            "Stakoron",
            "Styris",
            "Talani",
            "Tamus",
            "Tantalos",         // A king from a Greek tragedy
            "Tanuga",
            "Tarchannen",
            "Terosa",
            "Thera",            // A seldom encountered Dutch girl's name
            "Titan",            // The largest moon of Jupiter
            "Torin",            // A hero from Master of Magic
            "Triacus",
            "Turkana",
            "Tyrus",
            "Umberlee",         // A god from AD&D, which has a prominent role in Baldur's Gate
            "Utopia",           // The ultimate goal
            "Vadera",
            "Vagra",
            "Vandor",
            "Ventax",
            "Xenon",
            "Xerxes",           // A Greek hero
            "Yew",              // A city which is in almost all of the Ultima games
            "Yojimbo",          // A film by Akira Kurosawa
            "Zalkon",
            "Zuul"              // From the first Ghostbusters movie
        };
        return SolarSystemName;
    }
}
