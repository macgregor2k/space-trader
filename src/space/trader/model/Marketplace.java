package space.trader.model;

import java.io.*;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import space.trader.model.TradeGood.TradeGoodType;

/**
 * A class that deals with most of the logic for buying and selling TradeGoods
 *   at a planet's marketplace.
 * 
 * @author Robert
 */
public class Marketplace implements Serializable
{    
    private static final long serialVersionUID = 1L;
    private transient ObservableList<TradeGood> tradeGoods;
    
    /**
     * Instantiates a marketplace for a specific planet.
     * 
     * @param system The SolarSystem this marketplace is in.
     * @param rnd A Random that determines attributes of TradeGoods, such as
     *   their quantities.
     */
    public Marketplace(SolarSystem system, Random rnd)
    {
        tradeGoods = FXCollections.observableArrayList();
        
        for (TradeGoodType good : TradeGoodType.values())
        {
            TradeGood newGood = new TradeGood(good, system, rnd);
            if (newGood.getBuyPrice() > 0)
                tradeGoods.add(newGood);
        }
    }
    
    /**
     * Removes a quantity of TradeGoods from the Marketplace
     * 
     * @param good The TradeGood to be removed from the Marketplace.
     * @param quantity The quantity of the TradeGood in question to be removed.
     */
    public void removeGoodQuantity(TradeGood good, int quantity)
    {
        if (quantity <= good.getQuantity() && quantity > 0)
        {
            TradeGood syncItem = null; //added to avoid sync issues
            for(TradeGood item : tradeGoods)
            {
                if (item.equals(good))
                    syncItem = item;
            }
            if (syncItem != null)
            {
                syncItem.removeQuantity(quantity);
                if (syncItem.getQuantity() == 0)
                    tradeGoods.remove(syncItem);
            }
        }
        else
            throw new IndexOutOfBoundsException();
    }
    
    /**
     * Adds a quantity of TradeGoods into the Marketplace
     * 
     * @param good The TradeGood to be added to the Marketplace.
     * @param quantity The quantity of the TradeGood in question to be added.
     */
    public void addGoodQuantity(TradeGood good, int quantity)
    {
        if (quantity > 0)
        {
            if (tradeGoods.contains(good)) 
            {
                TradeGood syncItem = null; //added to avoid sync issues
                for(TradeGood item : tradeGoods)
                {
                    if (item.equals(good))
                        syncItem = item;
                }
                if (syncItem != null)
                    syncItem.addQuantity(quantity);
            } 
            else 
            {
                TradeGood newItem = new TradeGood(good);
                newItem.quantityProperty().set(quantity);
                tradeGoods.add(newItem);
            }
        }
    }
    
    /**
     * Gets the TradeGoods and their details that are currently in the
     *   Marketplace.
     * 
     * @return An ObservableList containing all the TradeGoods remaining in the
     *   Marketplace.
     */
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public ObservableList<TradeGood> getAvailableTradeGoods()
    {
        return tradeGoods;
    }
    
    // transient private final ObservableList<TradeGood> tradeGoods;
    private void writeObject(ObjectOutputStream os) throws IOException
    {
        os.defaultWriteObject();
        ArrayList<TradeGood> goods = new ArrayList<>();
        goods.addAll(tradeGoods);
        os.writeObject(goods);
    }
    
    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
    {
        is.defaultReadObject();
        tradeGoods = FXCollections.observableArrayList((ArrayList<TradeGood>)is.readObject());
    }
    
}
