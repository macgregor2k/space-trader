package space.trader.model;

import java.io.*;
import javafx.beans.property.*;
import space.trader.interfaceHelpers.IPlayer;
import space.trader.model.Ship.ShipType;

/**
 * Holds the information of the Player, such as how their skill points were
 *   allocated, their name, money, and remaining fuel.
 *
 * @author andrewamontree
 */
public class Player implements IPlayer, Serializable
{
    private static final double PLAYER_STARTING_CURRENCY = 100000.0;
    private static final long serialVersionUID = 1L;

    private final String name;
    transient private SimpleIntegerProperty pilotPoints;
    transient private SimpleIntegerProperty tacticalPoints;
    transient private SimpleIntegerProperty traderPoints;
    transient private SimpleIntegerProperty engineerPoints;
    transient private SimpleIntegerProperty investorPoints;
    transient private SimpleDoubleProperty currency;
    transient final private SimpleObjectProperty<PlayerShip> ship;

    /**
     * Instantiates a new player.
     *
     * @param name the name
     * @param pilotPoints the pilot points
     * @param tacticalPoints the tactical points
     * @param traderPoints the trader points
     * @param engineerPoints the engineer points
     * @param investorPoints the investor points
     */
    public Player(final String name, final int pilotPoints, final int tacticalPoints, 
            final int traderPoints, final int engineerPoints, final int investorPoints) 
    {
        this.name = name;
        this.pilotPoints = new SimpleIntegerProperty(pilotPoints);
        this.tacticalPoints = new SimpleIntegerProperty(tacticalPoints);
        this.traderPoints = new SimpleIntegerProperty(traderPoints);
        this.engineerPoints = new SimpleIntegerProperty(engineerPoints);
        this.investorPoints = new SimpleIntegerProperty(investorPoints);
        this.currency = new SimpleDoubleProperty(PLAYER_STARTING_CURRENCY);
        ship = new SimpleObjectProperty<PlayerShip>(new PlayerShip(ShipType.GNAT));
    }

    //Property accessors
    /**
     * The property holding the amount of skill points the player has in the
     *   piloting ability.
     *
     * @return A SimpleIntegerProperty holding the points that represent how
     *   skilled the player is for the piloting skill.
     */
    @Override
    public SimpleIntegerProperty pilotPointsProperty() { return pilotPoints; }

    /**
     * The property holding the amount of skill points the player has in the
     *   tactical ability.
     *
     * @return A SimpleIntegerProperty holding the points that represent how
     *   skilled the player is for the tactical skill.
     */
    @Override
    public SimpleIntegerProperty tacticalPointsProperty() { return tacticalPoints; }

    /**
     * The property holding the amount of skill points the player has in the
     *   trading ability.
     *
     * @return A SimpleIntegerProperty holding the points that represent how
     *   skilled the player is for the trading skill.
     */
    @Override
    public SimpleIntegerProperty traderPointsProperty() { return traderPoints; }

    /**
     * The property holding the amount of skill points the player has in the
     *   engineering ability.
     *
     * @return A SimpleIntegerProperty holding the points that represent how
     *   skilled the player is for the engineering skill.
     */
    @Override
    public SimpleIntegerProperty engineerPointsProperty() { return engineerPoints; }

    /**
     * The property holding the amount of skill points the player has in the
     *   investing ability.
     *
     * @return A SimpleIntegerProperty holding the points that represent how
     *   skilled the player is for the investing skill.
     */
    @Override
    public SimpleIntegerProperty investorPointsProperty() { return investorPoints; }

    /**
     * The property holding the amount of currency the player has.
     *
     * @return A SimpleDoubleProperty holding the player's current amount of money.
     */
    @Override
    public SimpleDoubleProperty currencyProperty() { return currency; }

    /**
     * The property holding the player's ship.
     *
     * @return The player's ship.
     */
    @Override
    public SimpleObjectProperty<PlayerShip> shipProperty() { return ship; }

    //Getters
    /**
     * Gets the player's name.
     *
     * @return A String containing the player's name.
     */
    @Override
    public String getName() { return name; }

    /**
     * Gets the amount of skill points the player has in the piloting ability.
     *
     * @return An int holding the points the player has in the piloting ability.
     */
    @Override
    public int getPilotPoints() { return pilotPoints.get(); }

    /**
     * Gets the amount of skill points the player has in the tactical ability.
     *
     * @return An int holding the points the player has in the tactical ability.
     */
    @Override
    public int getTacticalPoints() { return tacticalPoints.get(); }

    /**
     * Gets the amount of skill points the player has in the trading ability.
     *
     * @return An int holding the points the player has in the trading ability.
     */
    @Override
    public int getTraderPoints() { return traderPoints.get(); }

    /**
     * Gets the amount of skill points the player has in the engineering ability.
     *
     * @return An int holding the points the player has in the engineering ability.
     */
    @Override
    public int getEngineerPoints() { return engineerPoints.get(); }

    /**
     * Gets the amount of skill points the player has in the investing ability.
     *
     * @return An int holding the points the player has in the investing ability.
     */
    @Override
    public int getInvestorPoints() { return investorPoints.get(); }

    /**
     * Gets the amount of currency the player has.
     *
     * @return An int holding the amount of money the player has.
     */
    @Override
    public double getCurrency() { return currency.get(); }

    /**
     * Gets the player's Ship.
     *
     * @return A PlayerShip that has everything the player owns on it.
     */
    @Override
    public PlayerShip getShip() { return ship.get(); }

    //Setters
    /**
     * Sets the amount of skill points the player has in the piloting ability.
     *
     * @param pilotPoints The new amount of points for the piloting ability.
     */
    @Override
    public void setPilotPoints(final int pilotPoints) { this.pilotPoints.set(pilotPoints); }

    /**
     * Sets the amount of skill points the player has in the tactical ability.
     *
     * @param tacticalPoints The new amount of points for the tactical ability.
     */
    @Override
    public void setTacticalPoints(final int tacticalPoints) { this.tacticalPoints.set(tacticalPoints); }

    /**
     * Sets the amount of skill points the player has in the trading ability.
     *
     * @param traderPoints The new amount of points for the trading ability.
     */
    @Override
    public void setTraderPoints(final int traderPoints) { this.traderPoints.set(traderPoints); }

    /**
     * Sets the amount of skill points the player has in the engineering ability.
     *
     * @param engineerPoints The new amount of points for the engineering ability.
     */
    @Override
    public void setEngineerPoints(final int engineerPoints) { this.engineerPoints.set(engineerPoints); }

    /**
     * Sets the amount of skill points the player has in the investing ability.
     *
     * @param investorPoints The new amount of points for the investing ability.
     */
    @Override
    public void setInvestorPoints(final int investorPoints) { this.investorPoints.set(investorPoints); }

    /**
     * Sets the player's current ship to a new one.
     *
     * @param ship The player's new Ship.
     */
    @Override
    public void setShip(final PlayerShip ship) { this.ship.set(ship); }

    /**
     * Removes an amount of currency from the Player's money.
     * 
     * @param currency The amount to be removed from the Player's money.
     */
    @Override
    public void removeCurrency(final double currency)
    {
        if (currency <= this.currency.get() && currency >= 0) {
            this.currency.set(this.currency.get() - currency);
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Adds an amount of currency to the Player's money.
     * 
     * @param currency The amount to be added to the Player's money.
     */
    @Override
    public void addCurrency(final double currency)
    {
        this.currency.set(this.currency.get() + currency);
    }

    /**
     * The toString method returns the Player's name, along with the number of
     *   skill points that were allocated in each of the possible categories.
     * 
     * @return A String containing the Player's name and how the skill points
     *   were spent.
     */
    @Override
    public String toString()
    {
        return "Player Name: " + getName() + "\nPilot Points: " + getPilotPoints() +
                "\nTactical Points: " + getTacticalPoints() + "\nTrader Points: " + getTraderPoints() +
                "\nEngineerPoints: " + getEngineerPoints() + "\nInvestor Points: " + getInvestorPoints();
    }

    private void writeObject(final ObjectOutputStream output) throws IOException
    {
        output.defaultWriteObject();
        output.writeInt(pilotPoints.get());
        output.writeInt(tacticalPoints.get());
        output.writeInt(traderPoints.get());
        output.writeInt(engineerPoints.get());
        output.writeInt(investorPoints.get());
        output.writeDouble(currency.get());
    }

    private void readObject(final ObjectInputStream input) throws IOException, ClassNotFoundException
    {
        input.defaultReadObject();
        pilotPoints = new SimpleIntegerProperty(input.readInt());
        tacticalPoints = new SimpleIntegerProperty(input.readInt());
        traderPoints = new SimpleIntegerProperty(input.readInt());
        engineerPoints = new SimpleIntegerProperty(input.readInt());
        investorPoints = new SimpleIntegerProperty(input.readInt());
        currency = new SimpleDoubleProperty(input.readDouble());
    }
}
