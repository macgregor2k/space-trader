package space.trader.model;

import java.util.Collection;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author Robert
 */
public class PlayerShip extends Ship
{
    private final CargoBay cargoBay;
    private final GadgetBay gadgetBay;
    transient final private SimpleDoubleProperty fuel;

    public PlayerShip(final ShipType type)
    {
        super(type);
        cargoBay = new CargoBay(type.getCargoCapacity());
        gadgetBay = new GadgetBay(type.getEquipmentSlots());
        fuel = new SimpleDoubleProperty(type.getMaxFuel());
    }
    
    public PlayerShip(final ShipType type, final Collection<TradeGood> inventory, final int prevNumGoods,
            final Collection<Gadget> gadgets, final int previousNumEquipment)
    {
        super(type);
        cargoBay = new CargoBay(type.getCargoCapacity(), inventory, prevNumGoods);
        gadgetBay = new GadgetBay(type.getEquipmentSlots(), gadgets, previousNumEquipment);
        fuel = new SimpleDoubleProperty(type.getMaxFuel());
    }
    
    /**
      * Removes a certain amount of fuel from the Player's fuel.
      * 
      * @param fuel The amount of fuel to be removed.
      */
    public void removeFuel(final double fuel)
    {
        if (fuel <= this.fuel.get() && fuel >=0)
        {
            this.fuel.set(this.fuel.get() - fuel);
        }
        else
        {
            throw new IndexOutOfBoundsException();//TODO: proper error handling
        }
    }
    
    /**
     * Adds a certain amount of fuel to the Player's fuel.
     * 
     * @param fuel The amount of fuel to be added.
     */
    public void addFuel(final double fuel) { this.fuel.set(this.fuel.get() + fuel); }

    /**
     * Returns the PlayerShip's CargoBay.
     * 
     * @return The player's CargoBay.
     */
    public CargoBay getCargoBay() { return cargoBay; }

    /**
     * Returns the PlayerShip's GadgetBay.
     *
     * @return The player's GadgetBay.
     */
    public GadgetBay getGadgetBay() { return gadgetBay; }

    /**
     * Gets the property holding the player's current amount of fuel.
     *
     * @return A SimplDoubleProperty holding the player's fuel remaining.
     */
    public SimpleDoubleProperty fuelProperty() { return fuel; }

    /**
     * Gets the actual amount of fuel the player has left.
     *
     * @return A double holding the amount of fuel the player has left.
     */
    public double getFuel() { return fuel.get(); }
}
