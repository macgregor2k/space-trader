package space.trader.model;

import space.trader.interfaceHelpers.IPlanet.Tech_Level;

/**
 * A class holding the main details of a Ship.
 *
 * @author Jarrett
 */
public class Ship
{
    private ShipType type;
    private int hullPoints;
    
    public Ship(final ShipType type) 
    {
        this.type = type;
        hullPoints = type.getHullStrength();
    }

    /**
     * Returns the integrity of the ship's hull.
     *
     * @return An int holding the remaining heath of the ship.
     */
    public int getHullPoints() {
        return hullPoints;
    }

    /**
     * Removes points from the Ship's hull integrity.
     *
     * @param damagePoints The amount of damage the Ship just took.
     */
    public void damageShip(final int damagePoints)
    {
        //System.out.println("BEFORE");
        //System.out.println(hullPoints);
        //System.out.println(getType().hullStrength);
        hullPoints += damagePoints;
        //System.out.println("AFTER");
        //System.out.println(hullPoints);
        //System.out.println(getType().hullStrength);
        
    }

    /**
     * Repairs the Ship's hull a certain amount of points.
     *
     * @param repairPoints The amount the hull is repaired.
     */
    public void repairShip(final int repairPoints)
    {
        hullPoints += repairPoints;
        if (hullPoints > type.getHullStrength())
        {
            hullPoints = type.getHullStrength();
        }
    }
 
    /**
     * Gets the type of this Ship.
     * 
     * @return The ShipType of this Ship.
     */
    public ShipType getType() { return type; }

    /**
     * Sets the ShipType for this ship.  It is used to change the players ship
     *   when they purchase a new one.
     *
     * @param type The new ShipType of this ship.
     */
    public void setType(final ShipType type) 
    {
        this.type = type;
    }
    
    @SuppressWarnings("PublicInnerClass")
    public static enum ShipType
    {
        //Name, Cargo Slots, Weapon Slots, Shield Slots, Gadget Slots, Fuel, MinTechLevel, Price, HullStrength, EquipmentSlots
        FLEA ("FLEA", 10, 0, 0, 0, 2000.0/*MAX*/, Tech_Level.POST_INDUSTRIAL, 2000, 25, 0),
        GNAT ("GNAT", 15, 1, 0, 1, 1400.0, Tech_Level.HI_TECH, 10000, 100, 1),
        FIREFLY ("FIREFLY", 20, 1, 1, 1, 1700.0, Tech_Level.HI_TECH, 25000, 100, 2),
        MOSQUITO ("MOSQUITO", 15, 2, 1, 1, 1300.0, Tech_Level.HI_TECH, 30000, 100, 3),
        BUMBLEBEE ("BUMBLEBEE", 25, 1, 2, 2, 1500.0, Tech_Level.HI_TECH, 60000, 100, 4);

        
        private final String shipName;
        private final int cargoCapacity;
        private final int weaponSlots;
        private final int shieldSlots;
        private final int gadgetSlots;
        private final double maxFuel;
        private final Tech_Level minTechProduction;
        private final int buyCost;
        private final int sellCost;
        private final int hullStrength;
        private final int equipmentSlots;
        
        private ShipType(final String shipName, final int cargoCapacity, final int weaponSlots,
                final int shieldSlots, final int gadgetSlots, final double maxFuel,
                final Tech_Level minTechProduction, final int buyCost, final int hullStrength,
                final int equipmentSlots)
        {
            this.shipName = shipName;
            this.cargoCapacity = cargoCapacity;
            this.weaponSlots = weaponSlots;
            this.shieldSlots = shieldSlots;
            this.gadgetSlots = gadgetSlots;
            this.maxFuel = maxFuel;
            this.minTechProduction = minTechProduction;
            this.buyCost = buyCost;
            sellCost = buyCost; // TODO: We will want to change this later
            this.hullStrength = hullStrength;
            this.equipmentSlots = equipmentSlots;
        }
        
        /**
         * Gets the name of the type of ship.
         * 
         * @return A String holding the name of the type of this ship.
         */
        public String getShipName() { return shipName; }
        
        /**
         * Gets the maximum capacity for the cargo bay of this ship.
         * 
         * @return An int representing the maximum cargo capacity of this ship.
         */
        public int getCargoCapacity() { return cargoCapacity; }
        
        /**
         * Gets the maximum number of weapons this ship can have.
         * 
         * @return An int representing the maximum number of weapons this ship
         *   can have equipped at one time.
         */
        public int getWeaponSlots() { return weaponSlots; }
        
        /**
         * Gets the maximum number of shields this ship can have.
         * 
         * @return An int representing the maximum number of shields this ship
         *   can have equipped at one time.
         */
        public int getShieldSlots() {  return shieldSlots; }
        
        /**
         * Gets the maximum number of gadgets this ship can have.
         * 
         * @return An int representing the maximum number of gadgets this ship
         *   can have equipped at one time.
         */
        public int getGadgetSlots() { return gadgetSlots; }
        
        /**
         * Gets the maximum amount of fuel this ship can hold.
         * 
         * @return An double representing the maximum fuel capacity for this
         *   ship.
         */
        public double getMaxFuel() { return maxFuel; }
        
        /**
         * Gets the lowest Tech Level of planet that can produce this ship for
         *   sale.
         * 
         * @return The Tech_Level that is the lowest possible that can build
         *   and sell this ship.
         */
        public Tech_Level getMinTechProduction() { return minTechProduction; }
        
        /**
         * Gets the cost to buy this type of ship.
         * 
         * @return An int holding the amount of currency needed to purchase
         *   this type of ship from the ship yard.
         */
        public int getBuyCost() { return buyCost; }
        
        /**
         * Gets the amount the Player will get if they sell this type of ship.
         * 
         * @return An int holding the amount of currency they will get if they
         *   sell this type of ship to the ship yard.
         */
        public int getSellCost() { return sellCost; }
        
        /**
         * Gets the maximum integrity of this ships hull.
         * 
         * @return An int holding the maximum amount of damage this ship's
         *   hull can take before being destroyed.
         */
        public int getHullStrength() { return hullStrength; }
        
        /**
         * Gets the maximum amount of equipment this ship can have equipped at
         *   one time.
         * 
         * @return An int representing the number of slots this ship has for
         *   equipment.
         */
        public int getEquipmentSlots() { return equipmentSlots; }
        
        /**
         * This toString method returns the name of this ShipType.
         * 
         * @return A String holding the name of this ShipType.
         */
        @Override
        public String toString() { return shipName; }
    }
}
