package space.trader.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import space.trader.interfaceHelpers.IPlanet.Tech_Level;
import space.trader.model.Ship.ShipType;

/**
 *
 * @author Robert
 */
public class Shipyard
{
    final private ObservableList<Ship> shipTypes;
    
    public Shipyard(final SolarSystem system) {
        shipTypes = FXCollections.observableArrayList();
        
        for (ShipType ship : ShipType.values())
        {
            //TODO: Weird, make this more readable
            if (ship.getMinTechProduction().equals(Tech_Level.POST_INDUSTRIAL)
                    && (system.getTechLevel().equals(Tech_Level.POST_INDUSTRIAL)
                    || system.getTechLevel().equals(Tech_Level.HI_TECH))) 
            {
                final Ship newShip = new Ship(ship);
                shipTypes.add(newShip);
            }
            else if (system.getTechLevel().equals(Tech_Level.HI_TECH)) 
            {
                final Ship newShip = new Ship(ship);
                shipTypes.add(newShip);
            }
        }
    }
    
    public ObservableList<Ship> getAvailableShips() {
        return FXCollections.unmodifiableObservableList(shipTypes);
    }
}
