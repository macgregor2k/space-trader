package space.trader.model;

import java.io.*;
import java.util.Random;
import javafx.geometry.Point2D;
import space.trader.interfaceHelpers.IPlanet;

/**
 * A class that contains all the information for a SolarSystem, such as its
 *   location, name, and technology level.
 *
 * @author Robert
 */
@SuppressWarnings("serial")
public class SolarSystem implements IPlanet, Serializable
{
    transient private Point2D location;
    private final String name;
    private final Resource resource;
    private final Tech_Level techLevel;
    private final Marketplace market;
    private final GadgetStore gadgetStore;
    private final Shipyard shipyard;
    public static final int MAX_PRODUCTION_CAPACITY = 10;
    
    /**
     * Instantiates a new solar system.
     *
     * @param location The location of the new SolarSystem.
     * @param name The new SolarSystem's name.
     * @param rnd Determines some of the special attributes of this
     *   SolarSystem that make it unique.
     */
    public SolarSystem(final Point2D location, final String name, final Random rnd)
    {
        this.location = location;
        this.name = name;
        
        final Resource[] resources = Resource.values();
        this.resource = resources[rnd.nextInt(resources.length)];
        
        final Tech_Level[] techLevels = Tech_Level.values();
        this.techLevel = techLevels[rnd.nextInt(techLevels.length)];
        
        market = new Marketplace(this, rnd);
        gadgetStore = new GadgetStore(this);
        shipyard = new Shipyard(this);
    }
    
    //Getters
    /**
     * Gets the Marketplace that is unique to this SolarSystem.
     * 
     * @return The Marketplace of this SolarSystem.
     */
    @Override
    public Marketplace getMarket() { return market; }
    
     /**
     * Gets the GadgetStore that is unique to this SolarSystem.
     * 
     * @return The GadgetStore of this SolarSystem.
     */
    @Override
    public GadgetStore getGadgetStore() { return gadgetStore; }
    
    /**
     * Gets this SolarSystem's shipyard.
     * 
     * @return  The Shipyard of this SolarSystem.
     */
    public Shipyard getShipyard() { return shipyard; }
    
    /**
     * Gets the name of this SolarSystem.
     * 
     * @return The name of this SolarSystem.
     */
    @Override
    public String getName() { return name; }
    
    /**
     * Gets this SolarSystem's technology level.
     * 
     * @return The Tech_Level of this SolarSystem.
     */
    @Override
    public Tech_Level getTechLevel() { return techLevel; }
    
    /**
     * Gets this SolarSystem's unique resource.
     * 
     * @return The Resource that makes this SolarSystem unique.
     */
    @Override
    public Resource getResource() { return resource; }
    
    /**
     * Gets the location of this SolarSystem in the Galaxy.
     * 
     * @return A Point2D that represents the location of this SolarSystem.
     */
    @Override
    public Point2D getLocation() { return location; }

    /**
     * This toString method only returns the name of this SolarSystem.
     * 
     * @return A String holding the name of this SolarSystem.
     */
    @Override
    public String toString() { return name; }
    
    
    
    private void writeObject(final ObjectOutputStream output) throws IOException
    {
        output.defaultWriteObject();
        output.writeDouble(location.getX());
        output.writeDouble(location.getY());
    }
    
    private void readObject(final ObjectInputStream input) throws IOException, ClassNotFoundException
    {
        input.defaultReadObject();
        location = new Point2D(input.readDouble(), input.readDouble());
    }
}
