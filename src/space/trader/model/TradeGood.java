package space.trader.model;

import java.io.*;
import java.util.Objects;
import java.util.Random;
import javafx.beans.property.*;
import space.trader.interfaceHelpers.IPlanet.Tech_Level;

/**
 * The trade goods that can be bought, transported, and sold in order to make money
 * in the game.
 *
 * @author Robert
 */
public class TradeGood implements Serializable
{
    private static final long serialVersionUID = 1L;
    transient private SimpleObjectProperty<TradeGoodType> type;
    transient private SimpleIntegerProperty quantity;
    transient private ReadOnlyDoubleWrapper buyPrice;
    transient private ReadOnlyDoubleWrapper sellPrice;
    
    public TradeGood(TradeGoodType type, SolarSystem planet, Random rnd)
    {
        this.type = new SimpleObjectProperty<>(type);
        this.quantity = new SimpleIntegerProperty(rnd.nextInt(SolarSystem.MAX_PRODUCTION_CAPACITY - 1) + 1);
        this.buyPrice = new ReadOnlyDoubleWrapper(type.calculateBuyValue(planet, rnd));
        this.sellPrice = new ReadOnlyDoubleWrapper(type.calculateSellValue(planet, rnd));
    }
    
    public TradeGood(TradeGood good)
    {
        this.type = new SimpleObjectProperty<>(good.getType());
        this.quantity = new SimpleIntegerProperty(good.getQuantity());
        this.buyPrice = new ReadOnlyDoubleWrapper(good.getBuyPrice());
        this.sellPrice = new ReadOnlyDoubleWrapper(good.getSellPrice()); 
    }
    
    public TradeGood() {
        
    }
    
    //Properties
    /**
     * Accesses the property of this TradeGood's type.
     * 
     * @return The SimpleObjectProperty of this TradeGood's type.
     */
    public SimpleObjectProperty<TradeGoodType> typeProperty() { return type; }
    
    /**
     * Accesses the property of this TradeGood's quantity.
     * 
     * @return The SimpleIntegerProperty of this TradeGood's quantity.
     */
    public SimpleIntegerProperty quantityProperty() { return quantity; }
    
    /**
     * Accesses the property of this TradeGood's buy price.
     * 
     * @return The ReadOnlyDoubleProperty of this TradeGood's buy price.
     */
    public ReadOnlyDoubleProperty buyPriceProperty() { return buyPrice.getReadOnlyProperty(); }
    
    /**
     * Accesses the property of this TradeGood's sell price.
     * 
     * @return The ReadOnlyDoubleProperty of this TradeGood's sell price.
     */
    public ReadOnlyDoubleProperty sellPriceProperty() { return sellPrice.getReadOnlyProperty(); }
    
    //Getters
    /**
     * Gets the type of this TradeGood.
     * 
     * @return The TradeGoodType of this TradeGood.
     */
    public TradeGoodType getType() { return type.getValue(); }
    
    /**
     * Gets the quantity of this TradeGood.
     * 
     * @return An int representing the quantity of this TradeGood.
     */
    public int getQuantity() { return quantity.get(); }
    
    /**
     * Gets the buy price of this TradeGood.
     * 
     * @return A double representing the buy price of this TradeGood.
     */
    public double getBuyPrice() { return buyPrice.get(); }
    
    /**
     * Gets the sell price of this TradeGood.
     * 
     * @return A double representing the sell price of this TradeGood.
     */
    public double getSellPrice() { return sellPrice.get(); }
    
    //Setters
    /**
     * Increases the quantity of a TradeGood by a certain amount.
     * 
     * @param quantity The amount to add to the current quantity of this
     *   TradeGood.
     */
    public void addQuantity(int quantity) 
    { 
        this.quantity.set(this.quantity.get() + quantity); 
    }
    
    /**
     * Decreases the quantity of a TradeGood by a certain amount.
     * 
     * @param quantity The amount to remove from the current quantity of this
     *   TradeGood.
     */
    public void removeQuantity(int quantity) 
    { 
        this.quantity.set(this.quantity.get() - quantity);
        if (this.quantity.get() < 0)
            throw new IndexOutOfBoundsException();
    }
    
    /**
     * Compares this TradeGood with an Object to see if they are equal to each
     *   other.
     * 
     * @param o The Object to see if this TradeGood is equal to.
     * @return The boolean true if the parameter is equal to this TradeGood,
     *   and false it they are not equal.
     */
    @Override
    public boolean equals(Object o)
    {
        if (o == this)
            return true;
        
        if (o == null || o.getClass() != this.getClass())
            return false;
        
        return ((TradeGood)o).getType().equals(this.getType());
    }

    /**
     * Creates and returns the hash code for this TradeGood.
     * 
     * @return An int representing this TradeGood's hash code.
     */
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.type);
        return hash;
    }
    
    
    @SuppressWarnings("PublicInnerClass")
    public static enum TradeGoodType
    {
        WATER ("Water", Tech_Level.PRE_AGRICULTURE, Tech_Level.PRE_AGRICULTURE, 30, 3, 4),
        FURS ("Furs", Tech_Level.PRE_AGRICULTURE, Tech_Level.PRE_AGRICULTURE, 250, 10, 10),
        FOOD ("Food", Tech_Level.AGRICULTURE, Tech_Level.PRE_AGRICULTURE, 100, 5, 5),
        ORE ("Ore", Tech_Level.MEDIEVAL, Tech_Level.MEDIEVAL, 350, 20, 10),
        GAMES ("Games", Tech_Level.RENAISSANCE, Tech_Level.AGRICULTURE, 250, -10, 5),
        FIREARMS ("Firearms", Tech_Level.RENAISSANCE, Tech_Level.AGRICULTURE, 1250, -75, 75), //variance was 100
        MEDICINE ("Medicine", Tech_Level.EARLY_INDUSTRIAL, Tech_Level.AGRICULTURE, 650, -20, 10),
        MACHINES ("Machines", Tech_Level.EARLY_INDUSTRIAL, Tech_Level.RENAISSANCE, 900, -30, 5),
        NARCOTICS ("Narcotics", Tech_Level.INDUSTRIAL, Tech_Level.PRE_AGRICULTURE, 3500, -125, 99), //variance was 150
        ROBOTS ("Robots", Tech_Level.POST_INDUSTRIAL, Tech_Level.EARLY_INDUSTRIAL, 5000, -150, 75); //variance was 100
        
        private final String displayName;
        private final Tech_Level minTechProduction;
        private final Tech_Level minTechUsage;
        private final double basePrice;
        private final double priceLevelMultiplier;
        private final double priceVarianceMultiplier;
        
        private TradeGoodType(String displayName,Tech_Level MTLP, Tech_Level MTLU, 
                int BP, double PLM, double PVM)
        {
            this.displayName = displayName;
            this.minTechProduction = MTLP;
            this.minTechUsage = MTLU;
            this.basePrice = BP;
            this.priceLevelMultiplier = PLM;
            this.priceVarianceMultiplier = PVM;
        }
        
        /**
         * This toString method returns the name of this TradeGood.
         * 
         * @return A String holding the name of this TradeGood.
         */
        @Override
        public String toString()
        {
            return displayName;
        }
        
        //TODO: Separate buy and sell prices cause too much error. Rethink this maybe?
        
        //When arriving at a planet, you need to compute the price of goods at that planet. 
        //Look at the planets tech level (say it is Medieval = tech level 2). You could not 
        //buy or sell Machines or Robots here. The price for water would be 
        //30 (the base price) + 3*2 (the IPL * (Planet Tech Level - MTLP)) + (variance). 
        //We compute the variance by flipping a coin say heads price is increased, tails decreased. 
        //Since the water variance is 4, we would pick a random number 0-4. In our example, say we threw 
        //heads and got a 2. Our final price would be: 30 + (3*2) + (30*.2) = 30 + 6 + 6 = $42
        
        /**
         * Calculates the price to buy this TradeGood in this SolarSystem based
         *   on random numbers and the SolarSystem's technology level.
         * 
         * @param currentSystem The SolarSystem containing this TradeGood for
         *   sale.
         * @param rnd Helps determine the price variance of this TradeGood.
         * @return Buy value on this planet as a double, or -1 if not available.
         */
        public double calculateBuyValue(SolarSystem currentSystem, Random rnd)
        {
            double tradeValue = basePrice + (priceLevelMultiplier * 
                    (currentSystem.getTechLevel().ordinal() - minTechProduction.ordinal())) + 
                    (basePrice * rnd.nextInt((int) priceVarianceMultiplier + 1) * 0.01 * (rnd.nextBoolean() ? 1: -1));
            return (tradeValue >= 0 ? tradeValue : -1);
        }
        
        /**
         * Calculates the price to sell this TradeGood in this SolarSystem
         *   based on random numbers and the SolarSystem's technology level.
         * 
         * @param currentSystem The SolarSystem the Player is trying to sell
         *   this TradeGood to.
         * @param rnd Helps determine the price variance of this TradeGood.
         * @return Sell value on this planet as a double, or -1 if not available.
         */
        public double calculateSellValue(SolarSystem currentSystem, Random rnd)
        {
            double tradeValue = basePrice + (priceLevelMultiplier * 
                    (currentSystem.getTechLevel().ordinal() - minTechUsage.ordinal())) + 
                    (basePrice * rnd.nextInt((int) priceVarianceMultiplier + 1) * 0.01 * (rnd.nextBoolean() ? 1: -1));
            return (tradeValue >= 0 ? tradeValue : -1);
        }
    }

    public void setType(TradeGoodType type) {
        this.type = new SimpleObjectProperty<>(type);
    }
            
    private void writeObject(ObjectOutputStream os) throws IOException
    {
        os.defaultWriteObject();
        os.writeObject(type.get());
        os.writeInt(quantity.get());
        os.writeDouble(buyPrice.get());
        os.writeDouble(sellPrice.get());
    }
    
    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
    {
        is.defaultReadObject();
        type = new SimpleObjectProperty<>((TradeGoodType)is.readObject());
        quantity = new SimpleIntegerProperty(is.readInt());
        buyPrice = new ReadOnlyDoubleWrapper(is.readDouble());
        sellPrice = new ReadOnlyDoubleWrapper(is.readDouble()); 
    }
    
}
