package space.trader.model.events;

import java.util.Random;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author andrewamontree
 */
public abstract class AbstractEvent 
{ 
    private EventType type = null;
    protected final IPlayer player;
    
    public AbstractEvent(final EventType type, final IPlayer player) 
    {
        this.type = type;
        this.player = player;
    }
    
    @SuppressWarnings("PublicInnerClass")
    public static enum EventType
    {
            GAS_LEAK,
            DAMAGE_HULL,
            DEDUCT_CURRENCY,
            PIRATE_ATTACK
    }
    
    public abstract String generateEvent(Random rnd);
 
    public EventType getType()
    {
        return type;
    }
 
    public void setType(final EventType type) 
    {
        this.type = type;
    }
}
