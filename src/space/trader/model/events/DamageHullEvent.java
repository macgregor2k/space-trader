package space.trader.model.events;

import java.util.Random;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author andrewamontree
 */
public class DamageHullEvent extends AbstractEvent 
{
    public DamageHullEvent(final IPlayer player) 
    {
        super(AbstractEvent.EventType.DAMAGE_HULL, player);
    }
 
    @Override
    public String generateEvent(final Random rnd) 
    {
        return "You have encountered an asteroid field. Your ship has been damaged and your hull has lost 15 strength points";
    }
}
