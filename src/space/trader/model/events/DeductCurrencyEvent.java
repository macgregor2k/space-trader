package space.trader.model.events;

import java.util.Random;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author andrewamontree
 */
public class DeductCurrencyEvent extends AbstractEvent 
{
    public DeductCurrencyEvent(final IPlayer player) 
    {
        super(AbstractEvent.EventType.DEDUCT_CURRENCY, player);
    }
    
    @Override
    public String generateEvent(final Random rnd) 
    {
        int currency =  (int) player.getCurrency() / 2;
        currency = rnd.nextInt(currency);
        player.removeCurrency(currency);
        return "Hackers have obtained acess to your bank account and withdrawn funds. You have lost " + currency + " dollars";
    }
}
