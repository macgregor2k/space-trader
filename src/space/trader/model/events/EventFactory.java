package space.trader.model.events;

import java.util.Random;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author aamontree
 */
public class EventFactory
{
    private final static int MAX_EVENT_TYPES = 4;
    private final IPlayer player;
    private final Random rnd;
       
    public EventFactory(final Random rnd, final IPlayer player)
    {
        this.rnd = rnd;
        this.player = player;
    }
       
    @SuppressWarnings("UnusedAssignment")
    public AbstractEvent getEvent() 
    {
        AbstractEvent returnEvent = null;
        switch(rnd.nextInt(MAX_EVENT_TYPES))
        {
            case 0:
                returnEvent = new GasLeakEvent(player);
                break;
            case 1:
                returnEvent = new DeductCurrencyEvent(player);
                break;
            case 2:
                returnEvent = new DamageHullEvent(player);
                break;
            case 3:
                returnEvent = new PirateAttackEvent(player);
                break;
            default:
        }
        return returnEvent;
    }
}
