package space.trader.model.events;

import java.util.Random;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author andrewamontree
 */
public class GasLeakEvent extends AbstractEvent 
{
    public GasLeakEvent(final IPlayer player) 
    {
        super(AbstractEvent.EventType.GAS_LEAK, player);
    }
 
    @Override
    public String generateEvent(final Random rnd) {
        int fuel = (int) (player.getShip().getFuel() / 2);
        fuel = rnd.nextInt(fuel);
        player.getShip().removeFuel(fuel);
        return "There was a leak in your fuel tank. You have lost " + fuel + " gallons of fuel";
    }
}
