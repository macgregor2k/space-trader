package space.trader.model.events;

import java.util.Random;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author Jarrett
 */
public class PirateAttackEvent extends AbstractEvent {

    public PirateAttackEvent(final IPlayer player) {
        super(AbstractEvent.EventType.PIRATE_ATTACK, player);
    }

    @Override
    public String generateEvent(final Random rnd) {
        return "Pirates are approaching.  Prepare yourself for battle.";
    }
}
