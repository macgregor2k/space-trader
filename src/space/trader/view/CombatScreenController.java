package space.trader.view;

import java.util.Random;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.controlsfx.dialog.Dialogs;
import space.trader.MainGameController;
import space.trader.viewmodels.CombatVM;
import space.trader.interfaceHelpers.IPlayer;

/**
 *
 * @author Tyler and Jarrett
 */
public class CombatScreenController {
    //private Ship playerShip;
    //private Ship[] enemyShips;
    private CombatVM combatVM;
    private MainGameController gameControl;
    private Random rnd;
    
    private static final String RED_BAR    = "red-bar";
    private static final String YELLOW_BAR = "yellow-bar";
    private static final String ORANGE_BAR = "orange-bar";
    private static final String GREEN_BAR  = "green-bar";
    private static final String[] barColorStyleClasses = { RED_BAR, ORANGE_BAR, YELLOW_BAR, GREEN_BAR };
    
    @FXML
    private Button fightButton;
    @FXML
    private Label escapeChance;
    @FXML
    private Label playerHealth;
    @FXML
    private Label enemy1Health;
    @FXML
    private Label enemy2Health;
    @FXML
    private Label enemy3Health;
    @FXML
    private ProgressBar playerHealthBar;
    @FXML
    private ProgressBar enemy1HealthBar;
    @FXML
    private ProgressBar enemy2HealthBar;
    @FXML
    private ProgressBar enemy3HealthBar;
    @FXML
    private Circle playerOrb;
    @FXML
    private Circle enemy1Orb;
    @FXML
    private Circle enemy2Orb;
    @FXML
    private Circle enemy3Orb;
    @FXML
    private Label test;
    
    public CombatScreenController(IPlayer player, MainGameController gameControl) {
        this.gameControl = gameControl;
        combatVM = new CombatVM(player, gameControl);
        //playerShip = combatVM.getPlayer().getShip();
        rnd = new Random();
    }

    public void initialize()
    {
        //playerHealthBar = new ProgressBar(0.5);  //this is what broke the 
        //test = new Label();  //initialize method.  Can't call new.
        //test.setText("Initialized!");
        test.setText("");
        playerHealthBar.progressProperty().addListener(createBarListener(playerHealthBar));
        playerHealthBar.setProgress(combatVM.calcPlayerHealth());
        enemy1HealthBar.progressProperty().addListener(createBarListener(enemy1HealthBar));
        enemy1HealthBar.setProgress(1);
        enemy2HealthBar.progressProperty().addListener(createBarListener(enemy2HealthBar));
        enemy2HealthBar.setProgress(1);
        enemy3HealthBar.progressProperty().addListener(createBarListener(enemy3HealthBar));
        enemy3HealthBar.setProgress(1);
        
        escapeChance.setText("50%");
        playerHealth.setText(combatVM.getPlayerHealth() + "/" + combatVM.getMaxPlayerHealth());
        enemy1Health.setText(combatVM.getPirate1Health() + "/" + combatVM.getMaxPirateHealth());
        enemy2Health.setText(combatVM.getPirate2Health() + "/" + combatVM.getMaxPirateHealth());
        enemy3Health.setText(combatVM.getPirate3Health() + "/" + combatVM.getMaxPirateHealth());
    }

    public void fightIsClicked() {
        //System.out.println("ATTACKING");
        combatVM.playerAttacks();
        double[] piratesHealth = combatVM.calcPiratesHealth();
        enemy1HealthBar.setProgress(piratesHealth[0]);
        enemy2HealthBar.setProgress(piratesHealth[1]);
        enemy3HealthBar.setProgress(piratesHealth[2]);
        enemy1Health.setText(combatVM.getPirate1Health() + "/" + combatVM.getMaxPirateHealth());
        enemy2Health.setText(combatVM.getPirate2Health() + "/" + combatVM.getMaxPirateHealth());
        enemy3Health.setText(combatVM.getPirate3Health() + "/" + combatVM.getMaxPirateHealth());
        if (combatVM.pirate1IsDead()) {
            enemy1Orb.setFill(Color.BLACK);
        }
        if (combatVM.pirate2IsDead()) {
            enemy2Orb.setFill(Color.BLACK);
        }
        if (combatVM.pirate3IsDead()) {
            enemy3Orb.setFill(Color.BLACK);
        }
        if (combatVM.pirate1IsDead() && combatVM.pirate2IsDead() && combatVM.pirate3IsDead()) {
            Dialogs.create()
                .title("Victory!")
                .masthead("You Survived")
                .message("You were able to destroy the pirates.  You scavenged"
                        + " their wreckage and recived 10000 credits from the salvage")
                .showWarning();
            combatVM.loot();
            combatVM.escape();
        }
        
        combatVM.enemiesAttack();
        playerHealthBar.setProgress(combatVM.calcPlayerHealth());
        playerHealth.setText(combatVM.getPlayerHealth() + "/" + combatVM.getMaxPlayerHealth());
        if (combatVM.playerIsDead()) {
            playerOrb.setFill(Color.BLACK);
            playerHealth.setText(0 + "/" + combatVM.getMaxPlayerHealth());
            Dialogs.create()
                .title("Game Over")
                .masthead("You Died")
                .message("You were killed by the pirates.")
                .showWarning();
            combatVM.gameOver();
        }
        //System.out.println(combatVM.calcPlayerHealth());
    }

    public void escapeIsClicked() {
        if (rnd.nextInt(100) >= 50) {
            Dialogs.create()
                .title("Escape")
                .masthead("Success!")
                .message("You were able to successfully escape the pirates.")
                .showWarning();
            combatVM.escape();
        } else {
            Dialogs.create()
                .title("Escape")
                .masthead("Failure!")
                .message("You were unable to escape the pirates.")
                .showWarning();
            combatVM.enemiesAttack();
            playerHealthBar.setProgress(combatVM.calcPlayerHealth());
            playerHealth.setText(combatVM.getPlayerHealth() + "/" + combatVM.getMaxPlayerHealth());
            if (combatVM.playerIsDead()) {
                playerOrb.setFill(Color.BLACK);
                playerHealth.setText(0 + "/" + combatVM.getMaxPlayerHealth());
                Dialogs.create()
                    .title("Game Over")
                    .masthead("You Died")
                    .message("You were killed by the pirates.")
                    .showWarning();
                combatVM.gameOver();
            }
        }
    }

    public ChangeListener<Number> createBarListener(ProgressBar bar) {
        ChangeListener<Number> barListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double progress = newValue == null ? 0 : newValue.doubleValue();
                //System.out.println("Changed");
                if (progress < 0.2) {
                    setBarStyleClass(bar, RED_BAR);
                } else if (progress < 0.4) {
                    setBarStyleClass(bar, ORANGE_BAR);
                } else if (progress < 0.6) {
                    setBarStyleClass(bar, YELLOW_BAR);
                } else {
                    setBarStyleClass(bar, GREEN_BAR);
                }
            }
        };
        return barListener;
    }

    private void setBarStyleClass(ProgressBar bar, String barStyleClass) {
                bar.getStyleClass().removeAll(barColorStyleClasses);
                bar.getStyleClass().add(barStyleClass);
    }
}
