package space.trader.view;

import java.io.IOException;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import space.trader.MainGameController;
import space.trader.model.*;
import space.trader.viewmodels.CreateGameVM;

/**
 * FXML Controller class.
 *
 * @author Robert
 */
public class CreateNewGameController
{
    @FXML
    private Label pointsLeft;
    @FXML
    private Label nameError;
    @FXML
    private Label pilotingPoints;
    @FXML
    private Label tacticalPoints;
    @FXML
    private Label tradingPoints;
    @FXML
    private Label engineerPoints;
    @FXML
    private Label investingPoints;
    @FXML 
    private TextField traderName;
    
    final private CreateGameVM createVM;
    
    public CreateNewGameController()
    {
        createVM = null;
    }
    
    public CreateNewGameController(MainGameController gameControl)
    {
        createVM = new CreateGameVM(gameControl);
    }
    
    public void initialize()//this is not being called when it is loaded. WHY???
    {
        //pointsLeft = new Label();
        //nameError = new Label();
        pointsLeft.setText(Integer.toString(10));
        pointsLeft.setTextFill(Color.BLACK);
        nameError.setVisible(false);
    }
    
    public void begin() { //alternative to initialize
        pointsLeft = new Label();
        nameError = new Label();
        pointsLeft.setText(Integer.toString(10));
        pointsLeft.setTextFill(Color.BLACK);
        nameError.setVisible(false);
    }
    
    /**
     * Handle piloting minus.
     */
    @FXML
    private void handlePilotingMinus()
    {
        final int pilotingCurrent = Integer.parseInt(pilotingPoints.getText());
        if (pilotingCurrent <= 10)
        {
            pilotingPoints.setFont(new Font(36));
        }
        if (getPointsLeft() < 10 && pilotingCurrent > 0)
        {
            pilotingPoints.setText(Integer.toString(pilotingCurrent - 1));
            addPointLeft();
        }
    }
    
    /**
     * Handle piloting plus.
     */
    @FXML
    private void handlePilotingPlus()
    {
        final int pilotingCurrent = Integer.parseInt(pilotingPoints.getText());
        if (pilotingCurrent >= 9)
        {
            pilotingPoints.setFont(new Font(24));
        }
        if (getPointsLeft() > 0)
        {
            pilotingPoints.setText(Integer.toString(pilotingCurrent + 1));
        }
        subtractPointLeft();
    }
    
    /**
     * Handle tactical minus.
     */
    @FXML
    private void handleTacticalMinus()
    {
        final int tacticalCurrent = Integer.parseInt(tacticalPoints.getText());
        if (tacticalCurrent <= 10)
        {
            tacticalPoints.setFont(new Font(36));
        }
        if (getPointsLeft() < 10 && tacticalCurrent > 0)
        {
            tacticalPoints.setText(Integer.toString(tacticalCurrent - 1));
            addPointLeft();
        }
    }
    
    /**
     * Handle tactical plus.
     */
    @FXML
    private void handleTacticalPlus()
    {
        final int tacticalCurrent = Integer.parseInt(tacticalPoints.getText());
        if (tacticalCurrent >= 9)
        {
            tacticalPoints.setFont(new Font(24));
        }
        if (getPointsLeft() > 0)
        {
            tacticalPoints.setText(Integer.toString(tacticalCurrent + 1));
        }
        subtractPointLeft();
    }
    
    /**
     * Handle trading minus.
     */
    @FXML
    private void handleTradingMinus()
    {
        final int tradeCurrent = Integer.parseInt(tradingPoints.getText());
        if (tradeCurrent <= 10)
        {
            tradingPoints.setFont(new Font(36));
        }
        if (getPointsLeft() < 10 && tradeCurrent > 0)
        {
            tradingPoints.setText(Integer.toString(tradeCurrent - 1));
            addPointLeft();
        }
    }
    
    /**
     * Handle trading plus.
     */
    @FXML
    private void handleTradingPlus()
    {
        final int tradeCurrent = Integer.parseInt(tradingPoints.getText());
        if (tradeCurrent >= 9)
        {
            tradingPoints.setFont(new Font(24));
        }
        if (getPointsLeft() > 0)
        {
            tradingPoints.setText(Integer.toString(tradeCurrent + 1));
        }
        subtractPointLeft();
    }
    
    /**
     * Handle engineer minus.
     */
    @FXML
    private void handleEngineerMinus()
    {
        final int engineerCurrent = Integer.parseInt(engineerPoints.getText());
        if (engineerCurrent <= 10)
        {
            engineerPoints.setFont(new Font(36));
        }
        if (getPointsLeft() < 10 && engineerCurrent > 0)
        {
            engineerPoints.setText(Integer.toString(engineerCurrent - 1));
            addPointLeft();
        }
    }
    
    /**
     * Handle engineer plus.
     */
    @FXML
    private void handleEngineerPlus()
    {
        final int engineerCurrent = Integer.parseInt(engineerPoints.getText());
        if (engineerCurrent >= 9)
        {
            engineerPoints.setFont(new Font(24));
        }
        if (getPointsLeft() > 0)
        {
            engineerPoints.setText(Integer.toString(engineerCurrent + 1));
        }
        subtractPointLeft();
    }
    
    /**
     * Handle investing minus.
     */
    @FXML
    private void handleInvestingMinus()
    {
        final int investingCurrent = Integer.parseInt(investingPoints.getText());
        if (investingCurrent <= 10)
        {
            investingPoints.setFont(new Font(36));
        }
        if (getPointsLeft() < 10 && investingCurrent > 0)
        {
            investingPoints.setText(Integer.toString(investingCurrent - 1));
            addPointLeft();
        }
    }
    
    /**
     * Handle investing plus.
     */
    @FXML
    private void handleInvestingPlus()
    {
        final int investingCurrent = Integer.parseInt(investingPoints.getText());
        if (investingCurrent >= 9)
        {
            investingPoints.setFont(new Font(24));
        }
        if (getPointsLeft() > 0)
        {
            investingPoints.setText(Integer.toString(investingCurrent + 1));
        }
        subtractPointLeft();
    }
    
    /**
     * Handle create button.
     */
    @FXML
    private void handleCreateButton()
    {
        if (traderName.getText().equals(""))
        {
            nameError.setText("Please enter a name!");
            nameError.setVisible(true);
        }
        else if (getPointsLeft() > 0)
        {
            nameError.setText("Skill points remaining!");
            nameError.setVisible(true);
        }
        else
        {
            //TODO: decopulate
            final Player player = new Player(
                traderName.getText(),
                Integer.parseInt(pilotingPoints.getText()),
                Integer.parseInt(tacticalPoints.getText()),
                Integer.parseInt(tradingPoints.getText()),
                Integer.parseInt(engineerPoints.getText()),
                Integer.parseInt(investingPoints.getText()));
            final Galaxy galaxy = new Galaxy();
            
            createVM.startNewGame(player, galaxy);
        }
    }
    
    /**
     * Handle cancel button.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @FXML
    private void handleCancelButton() throws IOException
    {
        createVM.cancelNewGame();
    }
    
    /**
     * Handle trader name key type.
     */
    @FXML
    private void handleTraderNameKeyType()
    {
        if (nameError.visibleProperty().get())
        {
            nameError.setVisible(false);
        }
    }
    
    /**
     * Gets the points left.
     *
     * @return the points left
     */
    private int getPointsLeft()
    {
        return Integer.parseInt(pointsLeft.getText());
    }
    
    /**
     * Adds the point left.
     */
    public void addPointLeft()
    {
        int points = getPointsLeft();
        if (points < 10)
        {
            points++;
        }
        if (points > 0)
        {
            pointsLeft.setTextFill(Color.BLACK);
        }
        pointsLeft.setText(Integer.toString(points));
    }
    
    /**
     * Subtract point left.
     */
    private void subtractPointLeft()
    {
        int points = getPointsLeft();
        if (points > 0)
        {
            points--;
        }
        if (points == 0)
        {
            pointsLeft.setTextFill(Color.RED);
        }
        pointsLeft.setText(Integer.toString(points));
        if (getPointsLeft() == 0 && nameError.getText().equals("Skill points remaining!"))
        {
            nameError.setVisible(false);
        }
    }
}
