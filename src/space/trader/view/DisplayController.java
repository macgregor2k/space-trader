package space.trader.view;

import java.io.IOException;
import java.util.Random;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import space.trader.MainGameController;
import space.trader.SpaceTrader;
import space.trader.interfaceHelpers.*;

/**
 * A view-specific implementation of IDisplayControl that knows how to call each
 * screen, in this case using the FXML option of JavaFX.
 *
 * @author Robert
 */
public class DisplayController implements IDisplayControl
{
    private final Stage mainStage;
    
    public DisplayController(final Stage stage)
    {
        mainStage = stage;
    }

    @Override
    public void loadCreateGameScreen(final MainGameController gameControl)
    { 
        try
        {
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(new CreateNewGameController(gameControl));
            loader.setLocation(SpaceTrader.class.getResource("view/CreateNewGame.fxml"));
            final Pane createNewGameScene = loader.load();
            mainStage.setScene(new Scene(createNewGameScene));
            mainStage.setResizable(false);
            mainStage.setTitle("Space Trader! - Create New Game");
            mainStage.show();
        }
        catch (IOException ex)
        {
            System.err.println("Could not find CreateNewGame.fxml file");
            ex.printStackTrace();
        }
    }

    @Override
    public void loadMapTravelScreen(final IPlayer player, final IGalaxy galaxy, 
            final ReadOnlyObjectProperty<IPlanet> currentSystem,
            final MainGameController gameControl, final Random rnd)
    {
        try
        {
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(new MapTravelScreenController(player, galaxy, 
                    currentSystem, gameControl, rnd));
            
            loader.setLocation(SpaceTrader.class.getResource("view/MapTravelScreen.fxml"));
            final Pane startupScreenScene = loader.load();
            mainStage.setScene(new Scene(startupScreenScene));
            mainStage.setTitle("Space Trader!");
            mainStage.show();
        }
        catch (IOException ex)
        {
            System.err.println("Could not find MapTravelScreen.fxml file");
            ex.printStackTrace();
        }
    }

    @Override
    public void loadMarketplaceScreen(final IPlayer player, 
            final ReadOnlyObjectProperty<IPlanet> currentSystem, final MainGameController gameControl)
    {
        try
        {
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(new MarketplaceScreenController(player, currentSystem, gameControl));
            loader.setLocation(SpaceTrader.class.getResource("view/MarketplaceScreen.fxml"));
            final Pane marketScreenScene = loader.load();
            mainStage.setScene(new Scene(marketScreenScene));
            mainStage.setTitle("Space Trader - Marketplace");
            mainStage.show();
        }
        catch (IOException ex)
        {
            System.err.println("Could not find MarketplaceScreen.fxml file");
            ex.printStackTrace();
        }
    }

    @Override
    public void loadStartScreen(final MainGameController gameControl)
    {
        try
        {
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(new StartupScreenController(gameControl));
            loader.setLocation(SpaceTrader.class.getResource("view/StartupScreen.fxml"));
            final Pane startupScreenScene = loader.load();
            mainStage.setScene(new Scene(startupScreenScene));
            mainStage.setTitle("Space Trader!");
            mainStage.show();
        }
        catch (IOException ex)
        {
            System.err.println("Could not find StartupScreen.fxml file");
            ex.printStackTrace();
        }
    }

    @Override
    public void loadCombatScreen(final IPlayer player, final MainGameController gameControl)
    {
        try
        {
            final FXMLLoader loader = new FXMLLoader();
            loader.setController(new CombatScreenController(player, gameControl));
            loader.setLocation(SpaceTrader.class.getResource("view/CombatScreen.fxml"));
            final Pane combatScreenScene = loader.load();
            mainStage.setScene(new Scene(combatScreenScene));
            mainStage.setTitle("Space Trader - Pirate Attack!");
            mainStage.show();
        }
        catch (IOException ex)
        {
            System.err.println("Could not find CombatScreen.fxml file");
            ex.printStackTrace();
        }
    }
}
