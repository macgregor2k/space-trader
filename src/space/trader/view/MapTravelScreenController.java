package space.trader.view;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.*;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.controlsfx.dialog.Dialogs;
import space.trader.MainGameController;
import space.trader.viewmodels.MapTravelVM;
import space.trader.interfaceHelpers.*;

/**
 * FXML Controller class.
 *
 * @author Robert
 */
public class MapTravelScreenController
{
    private MapTravelVM mapVM;
    //private MainGameController gameControl;
    
    @FXML
    private ListView<IPlanet> solarSystemList;
    @FXML
    private Text currentPlanetName;
    @FXML
    private Text destinationPlanetName;
    @FXML
    private Text fuelAmount;
    @FXML
    private Text info;  
    
    public MapTravelScreenController(IPlayer player, IGalaxy galaxy, ReadOnlyObjectProperty<IPlanet> currentSystem,
            MainGameController gameControl, Random rnd)
    {
        //this.gameControl = gameControl;
        mapVM = new MapTravelVM(player, currentSystem, galaxy, gameControl, rnd);
    }

    public void initialize()
    {
        currentPlanetName.setText("Current Planet: \n" + mapVM.planet.get().getName());

        solarSystemList.setItems(mapVM.solarSystemsList);
        
        info.setText("Fuel Cost: " + mapVM.getInfo(mapVM.planet.get().getLocation(), mapVM.planet.get()));
        
        solarSystemList.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends IPlanet> observable,
                        IPlanet oldValue,
                        IPlanet newValue) -> 
                {
                    destinationPlanetName.setText((newValue != null) ? newValue.getName() : "<Empty>");
                    info.setText((newValue != null) ?
                        "Fuel Cost: " + mapVM.getInfo(newValue.getLocation(), newValue) : "Fuel Cost: 0");
                });
        
        fuelAmount.textProperty().bind(mapVM.getFuelAmount());
    }     
    
    @FXML
    private void handleMarketButton() throws IOException
    {
        mapVM.travelToMarket();
    }
    
    @FXML
    private void handleTravelButton() throws IOException
    {
        if (solarSystemList.getSelectionModel().getSelectedItem() != null) {
            String msg = mapVM.travelToPlanet(solarSystemList.getSelectionModel().getSelectedItem());
            if (msg != null) {
                Dialogs.create()
                    .title("Event!")
                    .masthead("Something happened!")
                    .message(msg)
                    .showWarning();
            }
            currentPlanetName.setText("Current Planet: \n" + mapVM.planet.get().getName());
            info.setText("Fuel Cost: " + mapVM.getInfo(mapVM.planet.get().getLocation(), mapVM.planet.get()));
            if (msg != null && msg.equals("Pirates are approaching."
                    + "  Prepare yourself for battle.")) {
                mapVM.pirateAttack();
            } else if (msg != null && msg.equals("You have encountered an"
                    + " asteroid field. Your ship has been damaged and your hull"
                    + " has lost 15 strength points")) {
                mapVM.asteroidDamage(-15);
            }
        }
    }
    
    @FXML
    private void saveGameMenu()
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Save Your Game");
        File saveFile = chooser.showSaveDialog(null);
        
        if(saveFile != null)
            mapVM.saveGame(saveFile);
    }
    
    @FXML
    private void openGameMenu()
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open Saved Game");
        File openFile = chooser.showOpenDialog(null);
        
        if(openFile != null)
            mapVM.openGame(openFile);
    }
    
    @FXML
    private void closeGame()
    {
        System.exit(0);  //TODO: Check if game saved before exiting, through viewmodel
    }
}
