package space.trader.view;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import space.trader.MainGameController;
import space.trader.interfaceHelpers.IPlanet;
import space.trader.interfaceHelpers.IPlayer;
import space.trader.model.Ship.ShipType;
import space.trader.model.*;
import space.trader.viewmodels.MarketplaceVM;

/**
 * FXML Controller class
 *
 * @author Robert
 */
public class MarketplaceScreenController
{
    private MarketplaceVM marketVM;
    private Ship playerShip;
    private IPlayer player;

    //Trade Goods
    @FXML
    private TableView<TradeGood> marketTableTG;
    @FXML
    private TableView<TradeGood> cargoTableTG;
    @FXML
    private Label itemSellTG;
    @FXML
    private TextField sellQuantityTG;
    @FXML
    private Label walletAmountTG;
    @FXML
    private Label cargoCapacity;
    @FXML
    private Label itemBuyTG;
    @FXML
    private TextField buyQuantityTG;

    //Gadgets
    @FXML
    private TableView<Gadget> gadgetStoreTable;
    @FXML
    private TableView<Gadget> gadgetBayTable;
    @FXML
    private Label gadgetSell;
    @FXML
    private TextField gadgetSellQuantity;
    @FXML
    private Label walletAmountGS;
    @FXML
    private Label gadgetBayCapacity;
    @FXML
    private Label gadgetBuy;
    @FXML
    private TextField gadgetBuyQuantity;

    //Shipyard Current Ship
    @FXML
    private TableView<Ship> shipYardTable;
    @FXML
    private Label currentHullStrength;
    @FXML
    private Label currentCargoBay;
    @FXML
    private Label currentMaxFuel;
    @FXML
    private Label currentWeaponSlots;
    @FXML
    private Label currentShieldSlots;
    @FXML
    private Label currentGadgetSlots;
    @FXML
    private Label tradeInPrice;

    //Shipyard Selected Ship
    @FXML
    private Label selectedHullStrength;
    @FXML
    private Label selectedCargoBay;
    @FXML
    private Label selectedMaxFuel;
    @FXML
    private Label selectedWeaponSlots;
    @FXML
    private Label selectedShieldSlots;
    @FXML
    private Label selectedGadgetSlots;
    @FXML
    private Label buyPrice;

    //Shipyard buy
    @FXML
    private Label totalCostSY;
     @FXML
    private Label walletAmountSY;

    //refueling pieces
    @FXML
    private Slider fuelSlider;
    @FXML
    private Label currentFuelAmount;
    @FXML
    private Label currentFuelCost;
    @FXML
    private Button purchaseFuelButton;

    public MarketplaceScreenController(IPlayer player,
            ReadOnlyObjectProperty<IPlanet> currentSystem, MainGameController gameControl)
    {
        this.player = player;
        marketVM = new MarketplaceVM(player, currentSystem, gameControl);
        playerShip = player.getShip(); 
    }

    public void initialize()
    {
        //Trade Goods
        setupMarketTableview();
        setupCargoTableview();
        walletAmountTG.textProperty().bind(player.currencyProperty().asString());
        //cargoCapacity.textProperty().bind(player.getShip().getCargoBay().capacityProperty().asString());
        cargoCapacity.setText(String.valueOf(player.getShip().getCargoBay().capacityProperty().getValue()
            - player.getShip().getCargoBay().getNumGoods()));
        
        //Gadgets
        setupGadgetStoreTableview();
        setupGadgetBayTableview();
        //gadgetBayCapacity.textProperty().bind(player.getShip().getGadgetBay().capacityProperty().asString());
        gadgetBayCapacity.setText(String.valueOf(player.getShip().getGadgetBay().capacityProperty()
            .getValue() - player.getShip().getGadgetBay().getNumGadgets()));
        walletAmountGS.textProperty().bind(player.currencyProperty().asString());

        //Ship Yard
        setupShipYardTableview();
        setCurrentShipLabels();
        
        //primes the labels for the refueling station
        currentFuelAmount.textProperty().bind(player.getShip().fuelProperty().asString("%.2f"));
        currentFuelCost.setText(marketVM.updateFuelCost(0.0));
        
        //binds an obserable to the slider so it updates fuel
        fuelSlider.valueProperty().addListener((observale,oldValue,newValue)->{
            currentFuelCost.setText(marketVM.updateFuelCost(newValue.doubleValue()));
        });
        
        walletAmountSY.textProperty().bind(player.currencyProperty().asString());
    }
    
    @SuppressWarnings("unchecked")
    private void setupMarketTableview()
    {
        marketTableTG.setItems(marketVM.getMarketGoods());

        //Setup the name/type column
        TableColumn<TradeGood, String> tradeGoodCol = new TableColumn<>("Good");
        tradeGoodCol.setCellValueFactory(new Callback<CellDataFeatures<TradeGood, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<TradeGood, String> t) {
                return new SimpleStringProperty(t.getValue().getType().toString());
            }
        });
        tradeGoodCol.setPrefWidth(marketTableTG.getPrefWidth() / 3);

        //Setup the price column
        TableColumn<TradeGood, String> priceCol = new TableColumn<>("Price");
        priceCol.setCellValueFactory(new Callback<CellDataFeatures<TradeGood, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<TradeGood, String> t) {
                return t.getValue().buyPriceProperty().asString("%.2f");
            }
        });
        priceCol.setPrefWidth(marketTableTG.getPrefWidth() / 3);

        //Setup the quantity column
        TableColumn<TradeGood, Integer> quantityCol = new TableColumn<>("Quantity");
        quantityCol.setCellValueFactory(new PropertyValueFactory<TradeGood, Integer>("quantity"));
        quantityCol.setPrefWidth(marketTableTG.getPrefWidth() / 3);

        //Add columns to market table view, then add selection listener to link to itemBuy label
        marketTableTG.getColumns().setAll(tradeGoodCol, priceCol, quantityCol);
        marketTableTG.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends TradeGood> observable,
                        TradeGood oldValue,
                        TradeGood newValue) -> 
                {
                    itemBuyTG.setText((newValue != null) ? newValue.getType().toString() : "<Empty>");
                });
    }
    
    @SuppressWarnings("unchecked")
    private void setupCargoTableview()
    {
        cargoTableTG.setItems(marketVM.getCargoInventory());

        //Setup the name/type column
        TableColumn<TradeGood, String> tradeGoodCargoCol = new TableColumn<>("Good");
        tradeGoodCargoCol.setCellValueFactory(new Callback<CellDataFeatures<TradeGood, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<TradeGood, String> t) {
                return new SimpleStringProperty(t.getValue().getType().toString());
            } 
        });
        tradeGoodCargoCol.setPrefWidth(cargoTableTG.getPrefWidth() / 3);
        
        //Setup the price column
        TableColumn<TradeGood, String> priceCargoCol = new TableColumn<>("Price");
        priceCargoCol.setCellValueFactory(new Callback<CellDataFeatures<TradeGood, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<TradeGood, String> t) {
                return t.getValue().sellPriceProperty().asString("%.2f");
            }
        });
        priceCargoCol.setPrefWidth(cargoTableTG.getPrefWidth() / 3);

        //Setup the quantity column
        TableColumn<TradeGood, Integer> quantityCargoCol = new TableColumn<>("Quantity");
        quantityCargoCol.setCellValueFactory(new PropertyValueFactory<TradeGood, Integer>("quantity"));
        quantityCargoCol.setPrefWidth(cargoTableTG.getPrefWidth() / 3);

        //Add columns to cargo table view, then add selection listener to link to itemSell label
        cargoTableTG.getColumns().setAll(tradeGoodCargoCol, priceCargoCol, quantityCargoCol);
        cargoTableTG.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends TradeGood> observable,
                        TradeGood oldValue,
                        TradeGood newValue) -> 
                {
                    itemSellTG.setText((newValue != null) ? newValue.getType().toString() : "<Empty>");
                });
    }
    
    @SuppressWarnings("unchecked")
    private void setupShipYardTableview() 
    {
        shipYardTable.setItems(marketVM.getShipList());
       
        //Setup ship name column
        TableColumn<Ship, String> shipTypeCol = new TableColumn<>("Ship");
        shipTypeCol.setCellValueFactory(new Callback<CellDataFeatures<Ship, String>, ObservableValue<String>>() 
        {
            public ObservableValue<String> call(CellDataFeatures<Ship, String> t) 
            {
                return new SimpleStringProperty(t.getValue().getType().getShipName());
            } 
        });
        shipTypeCol.setPrefWidth(shipYardTable.getPrefWidth() / 2);
        
        //Setup ship price column
        TableColumn<Ship, String> priceBuyCol = new TableColumn<>("Price");
        priceBuyCol.setCellValueFactory(new Callback<CellDataFeatures<Ship, String>, ObservableValue<String>>() 
        {
            public ObservableValue<String> call(CellDataFeatures<Ship, String> t) 
            {
                return new SimpleStringProperty(Integer.toString(t.getValue().getType().getSellCost()));
            }
        });
        priceBuyCol.setPrefWidth(shipYardTable.getPrefWidth() / 2);
        
        //Add columns to Ship List view, then add selection listener to update labels
        shipYardTable.getColumns().setAll(shipTypeCol, priceBuyCol);
        shipYardTable.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Ship> observable,
                        Ship oldValue,
                        Ship newValue) -> 
                {
                    ShipType newShip = newValue.getType();
                    selectedHullStrength.setText(Integer.toString(newShip.getHullStrength()));
                    selectedCargoBay.setText(Integer.toString(newShip.getCargoCapacity()));
                    selectedMaxFuel.setText(Double.toString(newShip.getMaxFuel()));
                    selectedWeaponSlots.setText(Integer.toString(newShip.getWeaponSlots()));
                    selectedShieldSlots.setText(Integer.toString(newShip.getShieldSlots()));
                    selectedGadgetSlots.setText(Integer.toString(newShip.getGadgetSlots()));
                    buyPrice.setText(Integer.toString(newShip.getBuyCost()));

                    totalCostSY.setText(Integer.toString(newShip.getBuyCost() - playerShip.getType().getSellCost()));
                });
        
        shipYardTable.getSelectionModel().selectFirst();
    }

    /**
     * Updates all of the labels displaying information about the player's
     *   current Ship after they have bought a new one.
     */
    private void setCurrentShipLabels()
    {
        ShipType ship = playerShip.getType();
        currentHullStrength.setText(Integer.toString(ship.getHullStrength()));
        currentCargoBay.setText(Integer.toString(ship.getCargoCapacity()));
        currentMaxFuel.setText(Double.toString(ship.getMaxFuel()));
        currentWeaponSlots.setText(Integer.toString(ship.getWeaponSlots()));
        currentShieldSlots.setText(Integer.toString(ship.getShieldSlots()));
        currentGadgetSlots.setText(Integer.toString(ship.getGadgetSlots()));
        tradeInPrice.setText(Integer.toString(ship.getSellCost()));
    }
    
    /**
     * Handles the logic behind selling an item to a market.
     * 
     * @param event The sell button being clicked.
     */
    @FXML
    private void clickSellTG(ActionEvent event) 
    {
        if (sellQuantityTG.getText().trim().length() != 0)
        {
            TradeGood good = cargoTableTG.getSelectionModel().getSelectedItem();
            int quantity = Integer.parseInt(sellQuantityTG.getText()); //TODO: need to handle exceptions
        
            marketVM.sellToMarket(good, quantity);
            cargoCapacity.setText(String.valueOf(player.getShip().getCargoBay().capacityProperty().getValue()
                - player.getShip().getCargoBay().getNumGoods()));
        }
    }

    /**
     * Handles the logic behind purchasing an item from a market.
     * 
     * @param event The buy button being clicked.
     */
    @FXML
    private void clickBuyTG(ActionEvent event) 
    {
        if(buyQuantityTG.getText().trim().length() != 0)
        {
            TradeGood good = marketTableTG.getSelectionModel().getSelectedItem();
            int quantity = Integer.parseInt(buyQuantityTG.getText()); //TODO: need to handle exceptions

            marketVM.buyFromMarket(good, quantity);
            cargoCapacity.setText(String.valueOf(player.getShip().getCargoBay().capacityProperty().getValue()
                - player.getShip().getCargoBay().getNumGoods()));
        }
    }
    
    @SuppressWarnings("unchecked")
    private void setupGadgetStoreTableview()
    {
        gadgetStoreTable.setItems(marketVM.getGadgetStoreGadgets());

        //Setup the name/type column
        TableColumn<Gadget, String> tradeGoodCol = new TableColumn<>("Good");
        tradeGoodCol.setCellValueFactory(new Callback<CellDataFeatures<Gadget, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Gadget, String> t) {
                return new SimpleStringProperty(t.getValue().getType().toString());
            }
        });
        tradeGoodCol.setPrefWidth(gadgetStoreTable.getPrefWidth() / 3);

        //Setup the price column
        TableColumn<Gadget, String> priceCol = new TableColumn<>("Price");
        priceCol.setCellValueFactory(new Callback<CellDataFeatures<Gadget, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Gadget, String> t) {
                return t.getValue().buyPriceProperty().asString("%.2f");
            }
        });
        priceCol.setPrefWidth(gadgetStoreTable.getPrefWidth() / 3);

        //Setup the quantity column
        TableColumn<Gadget, Integer> quantityCol = new TableColumn<>("Quantity");
        quantityCol.setCellValueFactory(new PropertyValueFactory<Gadget, Integer>("quantity"));
        quantityCol.setPrefWidth(gadgetStoreTable.getPrefWidth() / 3);

        //Add columns to gadget store table view, then add selection listener to link to itemBuy label
        gadgetStoreTable.getColumns().setAll(tradeGoodCol, priceCol, quantityCol);
        gadgetStoreTable.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Gadget> observable,
                        Gadget oldValue,
                        Gadget newValue) -> 
                {
                    gadgetBuy.setText((newValue != null) ? newValue.getType().toString() : "<Empty>");
                });
    }
    
    @SuppressWarnings("unchecked")
    private void setupGadgetBayTableview()
    {
        gadgetBayTable.setItems(marketVM.getGadgetBayInventory());

        //Setup the name/type column
        TableColumn<Gadget, String> gadgetBayGadgetCol = new TableColumn<>("Gadget");
        gadgetBayGadgetCol.setCellValueFactory(new Callback<CellDataFeatures<Gadget, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Gadget, String> t) {
                return new SimpleStringProperty(t.getValue().getType().toString());
            } 
        });
        gadgetBayGadgetCol.setPrefWidth(gadgetBayTable.getPrefWidth() / 3);
        
        //Setup the price column
        TableColumn<Gadget, String> priceGadgetBayGadgetCol = new TableColumn<>("Price");
        priceGadgetBayGadgetCol.setCellValueFactory(new Callback<CellDataFeatures<Gadget, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Gadget, String> t) {
                return t.getValue().sellPriceProperty().asString("%.2f");
            }
        });
        priceGadgetBayGadgetCol.setPrefWidth(gadgetBayTable.getPrefWidth() / 3);

        //Setup the quantity column
        TableColumn<Gadget, Integer> quantityGadgetBayGadgetCol = new TableColumn<>("Quantity");
        quantityGadgetBayGadgetCol.setCellValueFactory(new PropertyValueFactory<Gadget, Integer>("quantity"));
        quantityGadgetBayGadgetCol.setPrefWidth(gadgetBayTable.getPrefWidth() / 3);

        //Add columns to cargo table view, then add selection listener to link to itemSell label
        gadgetBayTable.getColumns().setAll(gadgetBayGadgetCol, priceGadgetBayGadgetCol, quantityGadgetBayGadgetCol);
        gadgetBayTable.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Gadget> observable,
                        Gadget oldValue,
                        Gadget newValue) -> 
                {
                    gadgetSell.setText((newValue != null) ? newValue.getType().toString() : "<Empty>");
                });
    }
    
    /**
     * Handles the logic behind selling an gadget to the gadget store.
     * 
     * @param event The sell button being clicked.
     */
    @FXML
    private void clickSellGadget(ActionEvent event)
    {
        if (gadgetSellQuantity.getText().trim().length() != 0)
        {
            Gadget good = gadgetBayTable.getSelectionModel().getSelectedItem();
            int quantity = Integer.parseInt(gadgetSellQuantity.getText()); //TODO: need to handle exceptions
        
            marketVM.sellToGadgetStore(good, quantity);
            gadgetBayCapacity.setText(String.valueOf(player.getShip().getGadgetBay().capacityProperty().getValue()
                - player.getShip().getGadgetBay().getNumGadgets()));
        }
    }

    /**
     * Handles the logic behind purchasing an gadget from a gadget store.
     * 
     * @param event The buy button being clicked.
     */
    @FXML
    private void clickBuyGadget(ActionEvent event)
    {
        if(gadgetBuyQuantity.getText().trim().length() != 0)
        {
            Gadget good = gadgetStoreTable.getSelectionModel().getSelectedItem();
            int quantity = Integer.parseInt(gadgetBuyQuantity.getText()); //TODO: need to handle exceptions

            marketVM.buyFromGadgetStore(good, quantity);
            gadgetBayCapacity.setText(String.valueOf(player.getShip().getGadgetBay().capacityProperty().getValue()
            - player.getShip().getGadgetBay().getNumGadgets()));
        }
    }
    
    /**
     * Handles the logic behind purchasing a new ship from a shipyard.
     * 
     * @param event The click on the button to confirm the purchase.
     */
    @FXML
    private void clickBuyShip(ActionEvent event)
    {
        Ship newShip = shipYardTable.getSelectionModel().getSelectedItem();
        marketVM.buyNewShip(newShip);
        playerShip = newShip;
        setCurrentShipLabels();
        gadgetBayCapacity.setText(String.valueOf(player.getShip().getGadgetBay().capacityProperty().getValue()
            - player.getShip().getGadgetBay().getNumGadgets()));
        cargoCapacity.setText(String.valueOf(player.getShip().getCargoBay().capacityProperty().getValue()
                - player.getShip().getCargoBay().getNumGoods()));
        totalCostSY.setText("0");
    }
    
    /**
     * Returns the Player to the map travel screen.
     * 
     * @param event The click on the button to go back to the map travel screen.
     * @throws java.io.IOException 
     */
    @FXML
    private void clickFinished(ActionEvent event) throws java.io.IOException 
    {
        marketVM.returnToSpace();
    }
    
    /**
     * Handles the pressing of refuel button by the player, and updates the remaining fuel and the cost displayed.
     * 
     * @param event The click on the button to buy more fuel for the player's Ship.
     */
    @FXML
    private void purchaseFuel(ActionEvent event)
    {
        marketVM.updatePlayerFuel(fuelSlider.getValue());
        currentFuelCost.setText(marketVM.updateFuelCost(fuelSlider.getValue()));
    }
}
