package space.trader.view;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.stage.FileChooser;
import space.trader.MainGameController;
import space.trader.viewmodels.StartScreenVM;

/**
 * The Class StartupScreenController.
 *
 * @author Robert
 */
public class StartupScreenController
{    
    private final StartScreenVM startVM;
    
    public StartupScreenController(MainGameController gameControl)
    {
        startVM = new StartScreenVM(gameControl);
    }
    
    /**
     * Handle button new game.
     *
     * @param event the event
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @FXML
    private void handleButtonNewGame(final ActionEvent event)
    {   
        startVM.createNewGameButton();
    }

    /**
     * Handle button continue.
     *
     * @param event the event
     */
    @FXML
    private void handleButtonContinue(final ActionEvent event) 
    {
        
        final FileChooser chooser = new FileChooser();
        chooser.setTitle("Open Saved Game");
        final File openFile = chooser.showOpenDialog(null);
        
        if(openFile != null)
        {
            startVM.openGame(openFile);
        }
    }

    /**
     * Handle button exit.
     *
     * @param event the event
     */
    @FXML
    private void handleButtonExit(final ActionEvent event) 
    {
       System.exit(0);  
    }
}
