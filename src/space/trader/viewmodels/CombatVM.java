
package space.trader.viewmodels;

import java.util.Random;
import space.trader.MainGameController;
import space.trader.interfaceHelpers.IPlayer;
import space.trader.model.Ship;

/**
 *
 * @author Tyler and Jarrett
 */
public class CombatVM {
    private final IPlayer player;
    private MainGameController gameControl;
    private final Ship playerShip;
    private final int pirateMaxHealth;
    private int pirate1Health;
    private int pirate2Health;
    private int pirate3Health;
    private int pirateDamage;
    private Random rnd;

    public CombatVM(IPlayer player, MainGameController gameControl) {
        this.player = player;
        this.gameControl = gameControl;
        playerShip = this.player.getShip();
        pirateMaxHealth = 50;
        pirate1Health = pirateMaxHealth;
        pirate2Health = pirateMaxHealth;
        pirate3Health = pirateMaxHealth;
        pirateDamage = -5;
        rnd = new Random();
    }

    public IPlayer getPlayer() {
        return player;
    }

    public int getPlayerHealth() {
        return playerShip.getHullPoints();
    }

    public int getMaxPlayerHealth() {
        return playerShip.getType().getHullStrength();
    }

    public int getMaxPirateHealth() {
        return pirateMaxHealth;
    }

    public int getPirate1Health() {
        return pirate1Health;
    }

    public int getPirate2Health() {
        return pirate2Health;
    }

    public int getPirate3Health() {
        return pirate3Health;
    }

    public double calcPlayerHealth() {
        return ((double) playerShip.getHullPoints()) / playerShip.getType().getHullStrength();
    }

    public double[] calcPiratesHealth() {
        double[] piratesHealth = new double[3];
        piratesHealth[0] = ((double) pirate1Health) / pirateMaxHealth;
        piratesHealth[1] = ((double) pirate2Health) / pirateMaxHealth;
        piratesHealth[2] = ((double) pirate3Health) / pirateMaxHealth;
        return piratesHealth;
    }

    public void playerAttacks() {
        if (!pirate1IsDead()) {
            if (rnd.nextInt(10) >= 3) {
                pirate1Health = pirate1Health - 10;
            }
        }
        if (!pirate2IsDead()) {
            if (rnd.nextInt(10) >= 3) {
                pirate2Health = pirate2Health - 10;
            }
        }
        if (!pirate3IsDead()) {
            if (rnd.nextInt(10) >= 3) {
                pirate3Health = pirate3Health - 10;
            }
        }
    }

    public void enemiesAttack() {
        if (pirate1Health > 0) {
            if (rnd.nextInt(10) >= 2) {
                playerShip.damageShip(pirateDamage);
            }
        }
        if (pirate2Health > 0) {
            if (rnd.nextInt(10) >= 2) {
                playerShip.damageShip(pirateDamage);
            }
        }
        if (pirate3Health > 0) {
            if (rnd.nextInt(10) >= 2) {
                playerShip.damageShip(pirateDamage);
            }
        }
    }

    public boolean playerIsDead() {
        return playerShip.getHullPoints() <= 0;
    }

    public boolean pirate1IsDead() {
        return pirate1Health <= 0;
    }

    public boolean pirate2IsDead() {
        return pirate2Health <= 0;
    }

    public boolean pirate3IsDead() {
        return pirate3Health <= 0;
    }

    public void loot() {
        player.addCurrency(10000);
    }

    public void escape() {
        gameControl.loadMapTravelScreen();
    }

    public void gameOver() {
        gameControl.start();
    }
}
