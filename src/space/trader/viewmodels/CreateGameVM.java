package space.trader.viewmodels;

import space.trader.MainGameController;
import space.trader.interfaceHelpers.IGalaxy;
import space.trader.interfaceHelpers.IPlayer;

/**
 * This class will act as an intermediate between the
 *   CreateNewGameController and the classes in the model package.
 *
 * @author Robert
 */
public class CreateGameVM
{
    private final MainGameController gameControl;

    public CreateGameVM(final MainGameController gameControl)
    {
        this.gameControl = gameControl;
    }
    
    /**
    * The method that tells the SpaceTrader class's MainGameController
    *   instance variable to change the screen being shown to the
    *   MapTravelScreen.
    *
    * @param player
    * @param galaxy
    */
    public void startNewGame(final IPlayer player, final IGalaxy galaxy)
    {
        gameControl.createNewGameStart(player, galaxy);
    }
    
    /**
    * The method that tells the SpaceTrader class's MainGameController
    *   instance variable to change the screen being shown to the
    *   StartupScreen.fxml.
    */
    public void cancelNewGame()
    {
        gameControl.start();
    }
}
