package space.trader.viewmodels;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.dialog.Dialogs;
import space.trader.MainGameController;
import space.trader.interfaceHelpers.*;
import space.trader.model.events.*;

/**
 * This class will act as an intermediate between the
 *   MapTravelScreenController and the classes in the model package.
 *
 * @author Wes
 */
public class MapTravelVM
{
    private final IPlayer player;
    private final MainGameController gameControl;
    private final Random rnd;

    public ReadOnlyObjectProperty<IPlanet> planet;
    public ObservableList<IPlanet> solarSystemsList;
    public final ReadOnlyStringWrapper fuelDisplay;

    public MapTravelVM(final IPlayer player, final ReadOnlyObjectProperty<IPlanet> currentSystem,
            final IGalaxy galaxy, final MainGameController gameControl, final Random rnd)
    {
        this.gameControl = gameControl;
        this.player = player;
        this.rnd = rnd;
        planet = currentSystem;

        //TODO: Rework
        fuelDisplay = new ReadOnlyStringWrapper();


        fuelDisplay.bindBidirectional(player.getShip().fuelProperty(), new NumberStringConverter());
        //

        solarSystemsList = galaxy.getSolarSystems();
    }

    /**
     * The getter method for the fuelDisplay instance variable.
     *
     * @return The value of the fuelDisplay instance variable as a
     *   ReadOnlyStringProperty instead of a ReadOnlyStringWrapper.
     */
    public ReadOnlyStringProperty getFuelAmount() { return fuelDisplay.getReadOnlyProperty(); }

    /**
     * The method that handles everything related to traveling to a new planet,
     *   such as updating the player's fuel and current location.
     *
     * @param solarSystem The SolarSystem the player is traveling to.
     * @return The event message
     * @throws IOException
     */
    public String travelToPlanet(final IPlanet solarSystem) throws IOException
    {
        if (!planet.equals(solarSystem)) {
        try
        {
            final double fuel = planet.get().getLocation().distance(solarSystem.getLocation());
            player.getShip().removeFuel(fuel);
            gameControl.travelToPlanet(solarSystem);

            if (rnd.nextDouble() <= 0.9)
            {
                final EventFactory factory = new EventFactory(rnd, player);
                final AbstractEvent presentEvent = factory.getEvent();
                if (presentEvent != null)
                {
                    return presentEvent.generateEvent(rnd);
                }
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            System.err.println("Index out of bounds exception when removing fuel");
        }
        }
        return null;
    }

    /**
     * A method that returns information about a planet, such as its location,
     *   distance from the planet the player is currently on, the fuel cost to
     *   get there, and the planet in question's tech level.
     *
     * @param selectedLocation The location of the planet that has been
     *   selected.
     * @param selected The currently selected planet
     * @return A String containing all of the information about the selected
     *   planet.
     */
    public String getInfo(final Point2D selectedLocation, final IPlanet selected)
    {
        final Point2D location = planet.get().getLocation();

        //double fuel = location.distance(selectedLocation);
        final double fuel = ((int)location.distance(selectedLocation) * 100) / 100.0; //set fuel to two decimal points

        return fuel + "\n" + "Clicked Location: "  + "(" +
                selectedLocation.getX() + ", " + selectedLocation.getY() + ")\n" +
                " Current Location: " + "(" + location.getX() + ", "
                + location.getY() + ")" + "\nTech Level: "
                + selected.getTechLevel();  //BUG with TECH LEVEL, needs to show selected planet's level, not current planet's.
    }

    /**
     * The method that tells the SpaceTrader class's MainGameController to
     *   change the screen being shown to MarketplaceScreen.fxml.
     *
     * @throws IOException
     */
    public void travelToMarket() throws IOException
    {
        gameControl.loadMarketplaceScreen();
    }

    public void pirateAttack() throws IOException {
        gameControl.loadCombatScreen();
    }

    public void asteroidDamage(int damage) {
        player.getShip().damageShip(damage);
    }

    /**
     * This works as a pass through function to move the correct
     * file to the save game back end implementation
     *
     * @param fileToSave
     */
    public void saveGame(final File fileToSave)
    {
        gameControl.saveGame(fileToSave);
    }
    /**
     * This works as a pass through function to move the correct
     * file to the load game back end implementation
     *
     * @param fileToOpen
     */
    public void openGame(final File fileToOpen)
    {
        gameControl.openGame(fileToOpen);
    }
}

