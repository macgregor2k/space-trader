package space.trader.viewmodels;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import space.trader.MainGameController;
import space.trader.interfaceHelpers.IPlanet;
import space.trader.interfaceHelpers.IPlayer;
import space.trader.model.*;

/**
 * This view model holds all the properties and methods to implement the Marketplace logic.
 * This includes three separate markets with similar functionality: Trade Goods, 
 * Ships, and Equipment.
 *
 * @author Robert
 */
public class MarketplaceVM
{
    //Trade Goods
    private final ObservableList<TradeGood> marketGoodsList;
    private ObservableList<TradeGood> cargoGoodsList;
    
    //Ship Yard
    private final ObservableList<Ship> shipList;
    
    //Gadgets
    private final ObservableList<Gadget> gadgetStoreList;
    private ObservableList<Gadget> gadgetBayList;
    
    //General fields
    private final IPlayer player;

    private final Marketplace market;
    private final GadgetStore gadgetStore;
    private final SimpleObjectProperty<PlayerShip> ship;
    private final CargoBay cargoBay;
    private final GadgetBay gadgetBay;
    private final MainGameController gameControl;
    
    public MarketplaceVM(final IPlayer player,
            final ReadOnlyObjectProperty<IPlanet> currentSystem, 
            final MainGameController gameControl)
    {
        this.gameControl = gameControl;
        this.player = player;
        market = currentSystem.get().getMarket();
        gadgetStore = currentSystem.get().getGadgetStore();
        ship = player.shipProperty();
        cargoBay = ship.get().getCargoBay();
        gadgetBay = ship.get().getGadgetBay();
        
        //Trade Goods
        marketGoodsList = market.getAvailableTradeGoods();
        cargoGoodsList = cargoBay.getInventory();
        
        //Ship Yard
        shipList = currentSystem.get().getShipyard().getAvailableShips();
        
        //Gadgets
        gadgetStoreList = gadgetStore.getAvailableGadgets();
        gadgetBayList = gadgetBay.getInventory();
    }

    /**
     * This returns a list of Market goods to populate the current marketplace of the planet the player is on
     * 
     * @return 
     */
    public ObservableList<TradeGood> getMarketGoods()
    {
        return FXCollections.unmodifiableObservableList(marketGoodsList);
    }
    
    /**
     * This returns a list of ship updates in the form of gadgets to populate the ship upgrade section of a planet 
     * 
     * @return 
     */
    public ObservableList<Gadget> getGadgetStoreGadgets()
    {
        return FXCollections.unmodifiableObservableList(gadgetStoreList);
    }

    /**
     * This returns a list of the current inventory held by the player in their cargo bay
     * 
     * @return 
     */
    public ObservableList<TradeGood> getCargoInventory()
    {
        return FXCollections.unmodifiableObservableList(cargoGoodsList);
    }
    
    /**
     * This returns the players currently installed gadgets on their ship
     * 
     * @return 
     */
    public ObservableList<Gadget> getGadgetBayInventory()
    {
        return FXCollections.unmodifiableObservableList(gadgetBayList);
    }
    
    /**
     * This returns a list of the ships for sale on the current planet
     * 
     * @return 
     */
    public ObservableList<Ship> getShipList()
    {
        return FXCollections.unmodifiableObservableList(shipList);
    }
            
    
    /**
    * Calculates the amount of currency required to buy quantity of good specified.
    * If enough currency, deducts it from player wallet then adds the goods to the
    * player's cargo bay and removes the appropriate quantity from the marketplace.
    * 
    * @param good
    * @param quantity 
    */
    public void buyFromMarket(final TradeGood good, final int quantity)
    {
        try
        {
            //CargoBay goes first to check against capacity
            cargoBay.addGoodQuantity(good, quantity);
            final double goodPrice = good.buyPriceProperty().get();
            player.removeCurrency(goodPrice * quantity);
            market.removeGoodQuantity(good, quantity);
        }
        catch (IndexOutOfBoundsException e)
        {
            System.err.println("Index out of bounds exception when buying goods");
        }
    }
    
    /**
     * Removes the quantity of the good specified from the player's cargo bay
     * and adds it to the marketplace, then adds the appropriate amount of 
     * currency to the player's wallet.
     * 
     * @param good
     * @param quantity 
     */
    public void sellToMarket(final TradeGood good, final int quantity)
    {
        try
        {   
           cargoBay.removeGoodQuantity(good, quantity);
           final double goodPrice = good.sellPriceProperty().get();
           player.addCurrency(goodPrice * quantity);
           market.addGoodQuantity(good, quantity);
        }
        catch (IndexOutOfBoundsException e)
        {
            System.err.println("Index out of bounds exception when selling goods");
        }
    }
    
   /**
    * Calculates the amount of currency required to buy quantity of gadget specified.
    * If enough currency, deducts it from player wallet then adds the gadgets to the
    * player's gadget bay and removes the appropriate quantity from the gadgetStoreplace.
    * 
    * @param gadget
    * @param quantity 
    */
    public void buyFromGadgetStore(final Gadget gadget, final int quantity)
    {
        try
        {
            //GadgetBay goes first to check against capacity
            gadgetBay.addGadgetQuantity(gadget, quantity);
            final double gadgetPrice = gadget.buyPriceProperty().get();
            player.removeCurrency(gadgetPrice * quantity);
            gadgetStore.removeGadgetQuantity(gadget, quantity);
        }
        catch (IndexOutOfBoundsException e)
        {
            System.err.println("Index out of bounds exception when buying gadgets");
        }
    }
    
    /**
     * Removes the quantity of the gadget specified from the player's gadget bay
     * and adds it to the gadgetStoreplace, then adds the appropriate amount of 
     * currency to the player's wallet.
     * 
     * @param gadget
     * @param quantity 
     */
    public void sellToGadgetStore(final Gadget gadget, final int quantity)
    {
        try
        {   
           gadgetBay.removeGadgetQuantity(gadget, quantity);
           final double gadgetPrice = gadget.sellPriceProperty().get();
           player.addCurrency(gadgetPrice * quantity);
           gadgetStore.addGadgetQuantity(gadget, quantity);
        }
        catch (IndexOutOfBoundsException e)
        {
            System.err.println("Index out of bounds exception when selling gadgets");
        }
    }
    
    /**
     * Checks to see if the Player has enough money to buy a new Ship, and if
     * so changes the Player's Ship to the new one.
     * 
     * @param newShip The Type of Ship the Player is trying to purchase.
     */
    public void buyNewShip(final Ship newShip) 
    {
        //Price of new ship - trade in value of current ship
        try {
            final double shipPrice = newShip.getType().getBuyCost()
                - ship.get().getType().getSellCost();
            player.removeCurrency(shipPrice);
            player.getShip().setType(newShip.getType());
            cargoBay.setCapacity(ship.getValue().getType().getCargoCapacity());
            cargoGoodsList = cargoBay.getInventory();
            gadgetBay.setCapacity(ship.getValue().
                getType().getEquipmentSlots());
            gadgetBayList = gadgetBay.getInventory();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("You do not have enough money.");
        }
    }
    
    /**
     * Command to leave the marketplace and return back to the map/travel screen
     */
    public void returnToSpace()
    {
        gameControl.loadMapTravelScreen();
    }
    
    /**
     * This function takes a double as a scaler that ranges between 0 and 1 as a double.
     * This double is used to multiple by the total fuel the ship has remaining to bring itself back to full capacity.
     * Once run, it will update the fuel according to this scaler multiplier, and charge the player.
     * 
     * @param amount 
     */
    public void updatePlayerFuel(final double amount)
    {
        final double topOffFuelAmount = (ship.get().getType().getMaxFuel() - ship.get().getFuel())*amount;
        player.getShip().addFuel(topOffFuelAmount);
        player.removeCurrency(topOffFuelAmount);
    }
    
    /**
     * This handles the updating of the current price to refuel the players ship as they
     * adjust to varying refuel levels.
     * 
     * @param multiplier
     * @return 
     */
    public String updateFuelCost(final double multiplier)
    {   
    
        final double topOffFuelAmount = (ship.get().getType().getMaxFuel() - ship.get().getFuel())*multiplier;
        return String.format("%.2f", topOffFuelAmount);
    }
}
