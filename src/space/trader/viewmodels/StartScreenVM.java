package space.trader.viewmodels;

import java.io.File;
import space.trader.MainGameController;

/**
 * This class will act as an intermediate between the 
 *   StartupScreenController and the classes in the model package.
 * 
 * @author Robert
 */
public class StartScreenVM
{
    private final MainGameController gameControl;
    
    /**
     * This sets the link to the main game controller.
     * 
     * @param gameControl 
     */
    public StartScreenVM(final MainGameController gameControl)
    {
        this.gameControl = gameControl;
    }
    
    /**
    * Changes the screen being displayed to the character 
    *   creation/customization screen (CreateNewGame.fxml).
    */
    public void createNewGameButton()
    {
        gameControl.loadCreateGameScreen();
    }
    
    /**
     * This acts as a pass through function that allows the file handle to be sent to the
     * game controller for back end loading support.
     * 
     * @param fileToOpen 
     */
    public void openGame(final File fileToOpen)
    {
        gameControl.openGame(fileToOpen);
    }
}
