package space.trader.model;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Tyler Serrian
 */
public class AddGadgetQuantityTest {
    Galaxy galaxy;
    private GadgetBay gadgetBay;
    private Gadget gadget;
    private int quantity;
    
    public AddGadgetQuantityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        galaxy = new Galaxy();
        gadget = new Gadget(Gadget.GadgetType.CLOAK, (SolarSystem) galaxy.getSolarSystems().get(0));
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test
    public void negativeQuantityTest() {
        gadgetBay = new GadgetBay(6);
        quantity = -1;
        thrown.expect(IndexOutOfBoundsException.class);
        gadgetBay.addGadgetQuantity(gadget, quantity);
        fail();
    }
    
    @Test
    public void zeroQuantityTest() {
        gadgetBay = new GadgetBay(6);
        quantity = 0;
        thrown.expect(IndexOutOfBoundsException.class);
        gadgetBay.addGadgetQuantity(gadget, quantity);
        fail();
    }
    
    @Test
    public void tooLargeQuantityTest() {
        gadgetBay = new GadgetBay(6);
        quantity = 7;
        thrown.expect(IndexOutOfBoundsException.class);
        gadgetBay.addGadgetQuantity(gadget, quantity);
        fail();
    }
    
    @Test
    public void maxQuantityTest() {
        gadgetBay = new GadgetBay(6);
        quantity = 6;
        gadgetBay.addGadgetQuantity(gadget, quantity);
        assertEquals(6, gadgetBay.getNumGadgets());
        for (int i = 0; i < gadgetBay.getInventory().size(); i++) {
            assertEquals(gadget, gadgetBay.getInventory().get(i));
        }
    }
    
    @Test
    public void minQuantityTest() {
        gadgetBay = new GadgetBay(6);
        quantity = 1;
        gadgetBay.addGadgetQuantity(gadget, quantity);
        assertEquals(1, gadgetBay.getNumGadgets());
        assertEquals(gadget, gadgetBay.getInventory().get(0));
    }
}
