package space.trader.model;

import org.junit.*;
import static org.junit.Assert.*;
import space.trader.model.Ship.ShipType;


/**
 *
 * @author Wesley Hughes
 */
public class PlayerShipTest {
    
    public PlayerShipTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of removeFuel method, of class PlayerShip.
     */
    @Test
    public void testRemoveFuel1() {
        System.out.println("removeFuel1");
        double fuel = 1.0;
        PlayerShip instance = new PlayerShip(ShipType.GNAT);
        instance.removeFuel(fuel);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(ShipType.GNAT.getMaxFuel()-1,instance.getFuel(),0.0);
    }    
    @Test
    public void testRemoveFuel2() {
        System.out.println("removeFuel2");
        double fuel = 0.0;
        PlayerShip instance = new PlayerShip(ShipType.GNAT);
        instance.removeFuel(fuel);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(ShipType.GNAT.getMaxFuel(),instance.getFuel(),0.0);
    }    
    @Test
    public void testRemoveFuel3() {
        System.out.println("removeFuel3");
        double fuel = 0.0;
        PlayerShip instance = new PlayerShip(ShipType.GNAT);
        instance.removeFuel(fuel);
        // TODO review the generated test code and remove the default call to fail.
        assertFalse(ShipType.GNAT.getMaxFuel()!=instance.getFuel());
    }   
    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveFuel4() {
        System.out.println("removeFuel4");
        
        PlayerShip instance = new PlayerShip(ShipType.GNAT);
        double fuel = instance.getFuel()+0.1;
        
        instance.removeFuel(fuel);
        // TODO review the generated test code and remove the default call to fail.
        
    }    
    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveFuel5() {
        System.out.println("removeFuel5");
        double fuel = -0.1;
        PlayerShip instance = new PlayerShip(ShipType.GNAT);
        instance.removeFuel(fuel);
        // TODO review the generated test code and remove the default call to fail.
        

    }
}