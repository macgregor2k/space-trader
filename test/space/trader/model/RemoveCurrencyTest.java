package space.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.*;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Jarrett Serrian
 */
public class RemoveCurrencyTest {
    private Player testPlayer;
    private double originalMoney;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        testPlayer = new Player("Tester", 2, 2, 2, 2, 2);
        originalMoney = testPlayer.getCurrency();
    }

    /**
     * Tests if Player's removeCurrency method throws an
     *   IndexOutOfBoundsException for negative values.
     */
    @Test
    public void testRemoveNegativeCurrency() {
        thrown.expect(IndexOutOfBoundsException.class);
        testPlayer.removeCurrency(-2000);
        fail();
    }

    /**
     * Tests if no money is added or removed when removing 0 currency.
     */
    @Test
    public void testRemove0Currency() {
        testPlayer.removeCurrency(0);
        assertEquals((float) originalMoney, (float) testPlayer.getCurrency(),
            0.0);
    }

    /**
     * Tests if it removes the correct amount for an arbitrary valid value.
     */
    @Test
    public void testRemoveValidCurrency() {
        testPlayer.removeCurrency(originalMoney / 2);
        assertEquals((float) originalMoney / 2, testPlayer.getCurrency(), 0.0);
    }

    /**
     * Tests if it correctly removes all the player's money.
     */
    @Test
    public void testRemoveMaxCurrency() {
        testPlayer.removeCurrency(originalMoney);
        assertEquals((float) 0, testPlayer.getCurrency(), 0.0);
    }

    /**
     * Tests if Player's removeCurrency method throws an
     *   IndexOutOfBoundsException for values greater than the player has.
     */
    @Test
    public void testRemoveTooMuchCurrency() {
        thrown.expect(IndexOutOfBoundsException.class);
        testPlayer.removeCurrency(originalMoney + 1);
        fail();
    }
}
