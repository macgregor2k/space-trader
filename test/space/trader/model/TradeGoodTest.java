package space.trader.model;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Andrew amontree
 */
public class TradeGoodTest {
    
    private TradeGood good;
    
    public TradeGoodTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {  

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        good = new TradeGood();
        good.setType(TradeGood.TradeGoodType.WATER);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class TradeGood.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        TradeGood goodCompare = new TradeGood();
        goodCompare.setType(TradeGood.TradeGoodType.WATER);
        Object o = goodCompare;
        TradeGood instance = good;
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }
}
