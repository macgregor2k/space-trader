package space.trader.view;

import java.lang.reflect.Field;
import javafx.scene.control.Label;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author Robert Roy
 */
public class CreateNewGameControllerTest
{
    
    public CreateNewGameControllerTest()
    {
    }
    
    @Test
    public void testaddPoints()
    {
        System.out.println("Test adding points");
        CreateNewGameController instance = new CreateNewGameController();
        instance.initialize();
        Label pointsLeft = new Label();
        try
        {
            Field pointsLeftLabel = instance.getClass().getDeclaredField("pointsLeft");
            pointsLeftLabel.setAccessible(true);
            pointsLeft = (Label)pointsLeftLabel.get(instance);
        }
        catch (Exception ex)
        {
            fail("Reflection exception when trying to access field");
        }
        
        assertEquals("Label should start at 10", 10, Integer.parseInt(pointsLeft.getText()));
        instance.addPointLeft();
        assertEquals("Label should remain at 10", 10, Integer.parseInt(pointsLeft.getText()));
        pointsLeft.setText("0");
        assertEquals("Label should be at 0", 0, Integer.parseInt(pointsLeft.getText()));
        pointsLeft.setText("1");
        instance.addPointLeft();
        assertEquals("Label should be at 1", 1, Integer.parseInt(pointsLeft.getText()));
    }
    
}
